package za.co.hellogroup.bas.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


public class DataSourceConfiguration {

	@Bean(name = "localds")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource primaryDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "local")
	@Autowired
	@Primary
    DataSourceTransactionManager primaryTransactionManager(@Qualifier("localds") DataSource datasource) {
		return new DataSourceTransactionManager(datasource);
	}
}
