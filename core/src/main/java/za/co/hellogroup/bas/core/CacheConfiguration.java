package za.co.hellogroup.bas.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Configuration
public class CacheConfiguration {

    private static Logger log = LoggerFactory.getLogger(CacheConfiguration.class.getClass());

    @Value("#{'${spring.cache.cache-names}'.split(',')}")
    private List<String> cacheList;

    @Value("#{'${spring.cache.redis.time-to-live}'.split(',')}")
    private List<String> cacheTtlList;

    @Bean
    RedisCacheWriter redisCacheWriter(JedisConnectionFactory factory) {
        return RedisCacheWriter.lockingRedisCacheWriter(factory);
    }

    @Bean
    public CacheManager buildUserLevelCache(RedisCacheWriter redisCacheWriter) {
        RedisCacheConfiguration defaultCacheConfig = RedisCacheConfiguration.defaultCacheConfig()
                .disableCachingNullValues();
        Map<String, RedisCacheConfiguration> cacheNamesConfigurationMap = new HashMap<>();
        int index = 0;
        for (String cache : cacheList) {
            log.info("Cache -> TTL : {},{}", cache, cacheTtlList.get(index));
            cacheNamesConfigurationMap.put(cache,
                    defaultCacheConfig.entryTtl(Duration.ofSeconds(Long.valueOf(cacheTtlList.get(index)))));
            index++;
        }
        return new RedisCacheManager(redisCacheWriter, defaultCacheConfig, cacheNamesConfigurationMap);

    }
}
