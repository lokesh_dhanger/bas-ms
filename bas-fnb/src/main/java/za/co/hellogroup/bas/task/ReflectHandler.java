package za.co.hellogroup.bas.task;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public abstract class ReflectHandler {
	public static String actualParametersToString(Class<?>[] pts) {
		StringBuffer result = new StringBuffer("");
		for (int i = 0; i < pts.length; i++) {
			result.append("p" + i);
			if (i < pts.length - 1) {
				result.append(",");
			}
		}

		return result.toString();
	}

	public static String classArrayToString(Class<?>[] pts) {
		StringBuffer result = new StringBuffer("");
		for (int i = 0; i < pts.length; i++) {
			result.append(getTypeName(pts[i]));
			if (i < pts.length - 1) {
				result.append(",");
			}
		}

		return result.toString();
	}

	public static String createCooperativeWrapper(Method m, String code1, String code2) {
		Class<?>[] pta = m.getParameterTypes();
		Class<?> retType = m.getReturnType();
		String fpl = formalParametersToString(pta);
		String apl = actualParametersToString(pta);
		Class<?>[] eTypes = m.getExceptionTypes();
		StringBuffer result = new StringBuffer(retType.getName() + " " + m.getName() + "(" + fpl + ")\n");
		if (0 != eTypes.length) {
			result.append("    throws " + classArrayToString(eTypes) + "\n");
		}
		result.append("{\n" + code1 + "    ");
		if (void.class != retType) {
			result.append(retType.getName() + " cooperativeReturnValue = ");
		}
		result.append("super." + m.getName() + "(" + apl + ");\n");
		result.append(code2);
		if (void.class != retType) {
			result.append("    return cooperativeReturnValue;\n");
		}
		result.append("}\n");

		return result.toString();
	}

	public static String createRenamedConstructor(Constructor<?> c, String name, String code) {
		Class<?>[] pta = c.getParameterTypes();
		String fpl = formalParametersToString(pta);
		String apl = actualParametersToString(pta);
		Class<?>[] eTypes = c.getExceptionTypes();
		StringBuffer result = new StringBuffer(name + "(" + fpl + ")\n");

		if (0 != eTypes.length) {
			result.append("    throws " + classArrayToString(eTypes) + "\n");
		}
		result.append("{\n    super(" + apl + ");\n" + code + "}\n");

		return result.toString();
	}

	public static String createReplacementMethod(Method m, String code) {
		Class<?>[] pta = m.getParameterTypes();
		String fpl = formalParametersToString(pta);
		Class<?>[] eTypes = m.getExceptionTypes();
		StringBuffer result = new StringBuffer(m.getName() + "(" + fpl + ")\n");

		if (0 != eTypes.length) {
			result.append("    throws " + classArrayToString(eTypes) + "\n");
		}
		result.append("{\n" + code + "}\n");
		return result.toString();
	}

	public static boolean equalsHeaderSuffixes(Method m1, Method m2) {
		if (m1.getReturnType() != m2.getReturnType()) {
			return false;
		}
		if (!Arrays.equals(m1.getExceptionTypes(), m2.getExceptionTypes())) {
			return false;
		}

		return equalSignatures(m1, m2);
	}

	public static boolean equalSignatures(Method m1, Method m2) {
		if (!m1.getName().equals(m2.getName())) {
			return false;
		}
		if (!Arrays.equals(m1.getParameterTypes(), m2.getParameterTypes())) {
			return false;
		}

		return true;
	}

	public static Field findField(Class<?> cls, String name)
			throws NoSuchFieldException {
		if (null != cls) {
			try {
				return cls.getDeclaredField(name);
			} catch (NoSuchFieldException _nsfe) {
				return findField(cls.getSuperclass(), name);
			}
		} else {
			throw new NoSuchFieldException();
		}
	}

	public static String formalParametersToString(Class<?>[] pts) {
		StringBuffer result = new StringBuffer("");

		for (int i = 0; i < pts.length; i++) {
			result.append(getTypeName(pts[i]) + " p" + i);
			if (i < pts.length - 1) {
				result.append(",");
			}
		}

		return result.toString();
	}

	public static String headerSuffixToString(Constructor<?> c) {
		StringBuffer header = new StringBuffer(signatureToString(c));
		Class<?>[] eTypes = c.getExceptionTypes();

		if (0 != eTypes.length) {
			header.append(" throws " + classArrayToString(eTypes));
		}

		return header.toString();
	}

	public static String headerSuffixToString(Method m) {
		StringBuffer header = new StringBuffer(getTypeName(m.getReturnType()) + " " + signatureToString(m));
		Class<?>[] eTypes = m.getExceptionTypes();

		if (0 != eTypes.length) {
			header.append(" throws " + classArrayToString(eTypes));
		}

		return header.toString();
	}

	public static String headerToString(Constructor<?> c) {
		StringBuffer mods = new StringBuffer(
				Modifier.toString(c.getModifiers()));
		if (0 == mods.length()) {
			return headerSuffixToString(c);
		} else {
			return mods.append(" ").append(headerSuffixToString(c)).toString();
		}
	}

	public static String headerToString(Method m) {
		StringBuffer mods = new StringBuffer(
				Modifier.toString(m.getModifiers()));
		if (0 == mods.length()) {
			return headerSuffixToString(m);
		} else {
			return mods.append(" ").append(headerSuffixToString(m)).toString();
		}
	}

	public static Class<?>[] getAllInterfaces(Class<?> cls, Class<?> limit) {
		AgentQueue cq = new AgentQueue(Class.class);
		if (cls.isInterface()) {
			cq.add(cls);
		}
		for (Class<?> x = cls; null != x && x != limit; x = x.getSuperclass()) {
			getInterfaceSubtree(x, cq);
		}

		return (Class[]) cq.toArray();
	}

	public static Class<?>[] getAllInterfaces(Class<?> cls) {
		return getAllInterfaces(cls, null);
	}

	public static Field[] getDeclaredIVs(Class<?> cls) {
		Field[] fields = cls.getDeclaredFields();
		// Count the IVs
		int numberOfIVs = 0;
		for (int i = 0; i < fields.length; i++) {
			if (!Modifier.isStatic(fields[i].getModifiers())) {
				numberOfIVs++;
			}
		}
		Field[] declaredIVs = new Field[numberOfIVs];
		// Populate declaredIVs
		int j = 0;
		for (int i = 0; i < fields.length; i++) {
			if (!Modifier.isStatic(fields[i].getModifiers())) {
				declaredIVs[j++] = fields[i];
			}
		}

		return declaredIVs;
	}

	public static Method[] getInstanceMethods(Class<?> cls) {
		List<Method> instanceMethods = new ArrayList<Method>();
		for (Class<?> c = cls; null != c; c = c.getSuperclass()) {
			Method[] methods = c.getDeclaredMethods();
			for (int i = 0; i < methods.length; i++) {
				if (!Modifier.isStatic(methods[i].getModifiers())) {
					instanceMethods.add(methods[i]);
				}
			}
		}
		Method[] ims = new Method[instanceMethods.size()];
		for (int j = 0; j < instanceMethods.size(); j++) {
			ims[j] = instanceMethods.get(j);
		}

		return ims;
	}

	public static Field[] getInstanceVariables(Class<?> cls) {
		List<Field> accum = new LinkedList<Field>();
		while (null != cls) {
			Field[] fields = cls.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				if (!Modifier.isStatic(fields[i].getModifiers())) {
					accum.add(fields[i]);
				}
			}
			cls = cls.getSuperclass();
		}
		Field[] retvalue = new Field[accum.size()];

		return accum.toArray(retvalue);
	}

	public static Method getMethod(Class<?> cls, String methodName, Class<?>[] parameterTypes) {
		for (Class<?> c = cls; null != c; c = c.getSuperclass()) {
			try {
				return c.getDeclaredMethod(methodName, parameterTypes);
			} catch (NoSuchMethodException nsme) {
				nsme.printStackTrace();
			}
		}
		Class<?>[] ca = getAllInterfaces(cls);
		for (int i = 0; i < ca.length; i++) {
			try {
				return ca[i].getDeclaredMethod(methodName, parameterTypes);
			} catch (NoSuchMethodException nsme) {
				nsme.printStackTrace();
			}
		}
		return null;
	}

	public static Method[] getMethodsLackingImplementation(Class<?> cls) {
		AgentQueue imq = selectMethods0(cls, 0, Modifier.ABSTRACT, null);
		AgentQueue amq = selectMethods0(cls, Modifier.ABSTRACT, 0, null);
		AgentQueue rmq = new AgentQueue(Method.class, equalSignaturesMethod);
		for (int i = 0; i < amq.size(); i++) {
			Method rm = (Method) amq.elementAt(i);
			if (!imq.contains(rm)) {
				rmq.add(rm);
			}
		}

		return (Method[]) rmq.toArray();
	}

	public static int getModifiersWithout(Method m, int unwantedModifiers) {
		int mods = m.getModifiers();

		return (mods ^ unwantedModifiers) & mods;
	}

	public static Class<?>[] getSuperclasses(Class<?> cls) {
		int i = 0;
		for (Class<?> x = cls.getSuperclass(); null != x; x = x.getSuperclass()) {
			i++;
		}
		Class<?>[] result = new Class[i];
		i = 0;
		for (Class<?> x = cls.getSuperclass(); null != x; x = x.getSuperclass()) {
			result[i++] = x;
		}

		return result;
	}

	public static Field[] getSupportedIVs(Class<?> cls) {
		if (null == cls) {
			return new Field[0];
		} else {
			Field[] inheritedIVs = getSupportedIVs(cls.getSuperclass());
			Field[] declaredIVs = getDeclaredIVs(cls);
			Field[] supportedIVs = new Field[declaredIVs.length
					+ inheritedIVs.length];
			System.arraycopy(declaredIVs, 0, supportedIVs, 0,
					declaredIVs.length);
			System.arraycopy(inheritedIVs, 0, supportedIVs, declaredIVs.length,
					inheritedIVs.length);

			return supportedIVs;
		}
	}

	public static Method getSupportedMethod(Class<?> cls, String name, Class<?>[] paramTypes) throws NoSuchMethodException {
		if (null == cls) {
			throw new NoSuchMethodException();
		}
		try {
			return cls.getDeclaredMethod(name, paramTypes);
		} catch (NoSuchMethodException _nsme) {
			return getSupportedMethod(cls.getSuperclass(), name, paramTypes);
		}
	}

	public static Method[] getSupportedMethods(Class<?> cls, Class<?> limit) {
		Vector<Method> supportedMethods = new Vector<Method>();
		for (Class<?> c = cls; c != limit; c = c.getSuperclass()) {
			Method[] methods = c.getDeclaredMethods();
			for (int i = 0; i < methods.length; i++) {
				boolean found = false;
				for (int j = 0; j < supportedMethods.size(); j++) {
					if (equalSignatures(methods[i],
							(Method) supportedMethods.elementAt(j))) {
						found = true;
						break;
					}
				}
				if (!found) {
					supportedMethods.add(methods[i]);
				}
			}
		}
		Method[] mArray = new Method[supportedMethods.size()];
		for (int k = 0; k < mArray.length; k++) {
			mArray[k] = supportedMethods.elementAt(k);
		}

		return mArray;
	}

	public static Method[] getSupportedMethods(Class<?> cls) {
		return getSupportedMethods(cls, null);
	}

	public static String getTypeName(Class<?> cls) {
		if (!cls.isArray()) {
			return cls.getName();
		} else {
			return getTypeName(cls.getComponentType()) + "[]";
		}
	}

	public static Method getUniquelyNamedMethod(Class<?> cls, String mName) {
		Method result = null;
		Method[] mArray = cls.getDeclaredMethods();
		for (int i = 0; i < mArray.length; i++) {
			if (mName.equals(mArray[i].getName())) {
				if (null == result) {
					result = mArray[i];
				} else {
					throw new RuntimeException("name is not unique");
				}
			}
		}
		return result;
	}

	public static Class<?>[] selectAncestors(Class<?> cls, int mustHave,
			int mustNotHave, Class<?> limit) {
		AgentQueue cq = new AgentQueue(Class.class);
		if (!cls.isInterface()) {
			for (Class<?> x = cls; x != limit; x = x.getSuperclass()) {
				int mods = x.getModifiers();
				if (((mods & mustHave) == mustHave)
						&& (0 == (mods & mustNotHave))) {
					cq.add(x);
				}
			}
		}
		Class<?>[] ca = getAllInterfaces(cls, limit);
		for (int i = 0; i < ca.length; i++) {
			int mods = ca[i].getModifiers();
			if (((mods & mustHave) == mustHave) && (0 == (mods & mustNotHave))) {
				cq.add(ca[i]);
			}
		}
		return (Class[]) cq.toArray();
	}

	public static Class<?>[] selectAncestors(Class<?> cls, int mustHave,
			int mustNotHave) {
		return selectAncestors(cls, mustHave, mustNotHave, null);
	}

	public static Field[] selectFields(Class<?> cls, int mustHave,
			int mustNotHave) {
		AgentQueue fq = new AgentQueue(Field.class);
		Class<?>[] ca = selectAncestors(cls, 0, 0);
		for (int j = 0; j < ca.length; j++) {
			Field[] fa = ca[j].getDeclaredFields();
			for (int i = 0; i < fa.length; i++) {
				int mods = fa[i].getModifiers();
				if (((mods & mustHave) == mustHave)
						&& (0 == (mods & mustNotHave))) {
					fq.add(fa[i]);
				}
			}
		}

		return (Field[]) fq.toArray();
	}

	public static Method[] selectMethods(Class<?> cls, int mustHave,
			int mustNotHave) {
		return (Method[]) selectMethods0(cls, mustHave, mustNotHave, null)
				.toArray();
	}

	public static Method[] selectMethods(Class<?> cls, int mustHave,
			int mustNotHave, Class<?> limit) {
		return (Method[]) selectMethods0(cls, mustHave, mustNotHave, limit)
				.toArray();
	}

	public static String signatureToString(Constructor<?> c) {
		return c.getName() + "("
				+ formalParametersToString(c.getParameterTypes()) + ")";
	}

	public static String signatureToString(Method m) {
		return m.getName() + "("
				+ formalParametersToString(m.getParameterTypes()) + ")";
	}

	static {
		Class<?>[] fpl = { Method.class, Method.class };
		try {
			equalSignaturesMethod = ReflectHandler.class.getMethod(
					"equalSignatures", fpl);
		} catch (NoSuchMethodException _nsme) {
			throw new RuntimeException(_nsme);
		}
	}

	private static void getInterfaceSubtree(Class<?> cls, AgentQueue cq) {
		Class<?>[] iArray = cls.getInterfaces();
		for (int j = 0; j < iArray.length; j++) {
			cq.add(iArray[j]);
			getInterfaceSubtree(iArray[j], cq);
		}
	}

	private static AgentQueue selectMethods0(Class<?> cls, int mustHave,
			int mustNotHave, Class<?> limit) {
		AgentQueue mq = new AgentQueue(Method.class, equalSignaturesMethod);
		Class<?>[] ca = selectAncestors(cls, 0, 0, limit);
		for (int j = 0; j < ca.length; j++) {
			Method[] ma = ca[j].getDeclaredMethods();
			for (int i = 0; i < ma.length; i++) {
				int mods = ma[i].getModifiers();
				if (((mods & mustHave) == mustHave)
						&& (0 == (mods & mustNotHave))) {
					mq.add(ma[i]);
				}
			}
		}

		return mq;
	}

	private static Method equalSignaturesMethod;
}
