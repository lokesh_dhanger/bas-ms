package za.co.hellogroup.bas.dao;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmailDomain {

	private String host;
	private String port;
	private String user;
	private String password;
	private String from;
	private boolean isAuth;
	private boolean isStartTls;
	private boolean isSslTrust;
	private String timeout;
	private String connectiontimeout;


}