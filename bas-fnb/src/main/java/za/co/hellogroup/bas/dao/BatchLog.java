package za.co.hellogroup.bas.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "batch_log")
public class BatchLog implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "on_off")
	private int onOff;

	@Column(name = "date_time")
	private String dateTime;

	@Column(name = "description")
	private String description;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BatchLog batchLog = (BatchLog) o;
		return onOff == batchLog.onOff && Objects.equals(id, batchLog.id) && Objects.equals(name, batchLog.name) && Objects.equals(dateTime, batchLog.dateTime) && Objects.equals(description, batchLog.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, onOff, dateTime, description);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("BatchLog{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", onOff=").append(onOff);
		sb.append(", dateTime='").append(dateTime).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}
}