package za.co.hellogroup.bas.service;

public interface CommonServiceDB {

    public boolean validateRefNumber(String refNumber) throws Exception;
    public String selectTransactionTypeGroup(String basType, String transactionTypeMain, String transactionTypeSub) throws Exception;

}
