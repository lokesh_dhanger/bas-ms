package za.co.hellogroup.bas.controller;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.hellogroup.bas.dao.BatchLog;
import za.co.hellogroup.bas.model.BaseRequestDataModel;
import za.co.hellogroup.bas.model.BaseResponseDataModel;
import za.co.hellogroup.bas.model.ServerStatusModel;
import za.co.hellogroup.bas.model.TaskLog;
import za.co.hellogroup.bas.service.BatchLogService;
import za.co.hellogroup.bas.service.DaemonService;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.util.LogUtil;

@RestController
@RequestMapping("fnb")
public class TaskController {

    final static Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private DaemonService daemonService;

    @Autowired
    private BatchLogService batchLogService;


    @PostMapping(value = "/agentstart")
    public String startTask(@RequestBody BaseRequestDataModel<BatchLog> params) {
        logger.info("startTask fnb request param : {}, {}", params.toString(), params.getData().toString());

        BaseResponseDataModel<ServerStatusModel> responseModel = new BaseResponseDataModel<>();
        ServerStatusModel serverStatusModel = null;

        daemonService.startAgentTimer(params.getData().getName());

        TaskLog taskLog = daemonService.getTaskLog(params.getData().getName());
        if (taskLog != null && taskLog.getIsAlive()) {
            responseModel.setResponseCode(CommonConstants.MC_SUCCESS_BAS);
            serverStatusModel = new ServerStatusModel();
            serverStatusModel.setStartTime(taskLog.getStartDateTime());

            try {
                // Get last stop time
                BatchLog paramDomain = new BatchLog();
                paramDomain.setName(params.getData().getName());
                paramDomain.setOnOff(CommonConstants.ON_OFF_OFF);
                BatchLog batchLogDomain = batchLogService.selectBatchLog(paramDomain);
                if (batchLogDomain != null) {
                    serverStatusModel.setStopTime(batchLogDomain.getDateTime());
                }

                // Insert batch log
                paramDomain.setOnOff(CommonConstants.ON_OFF_ON);
                paramDomain.setDateTime(taskLog.getStartDateTime());
                batchLogService.insertBatchLog(paramDomain);
            } catch (Exception e) {
                responseModel.setResponseMessage("startTask batch log exception: "+ LogUtil.getExceptionLog(e));
                logger.info("startTask batch log exception: {}", LogUtil.getExceptionLog(e));
            }
        } else {
            responseModel.setResponseCode(CommonConstants.MC_800);
        }
        responseModel.setData(serverStatusModel);
        return new Gson().toJson(responseModel);
    }

    @PostMapping(value = "/agentstop")
    public String stopTask(@RequestBody BaseRequestDataModel<BatchLog> params) {
        logger.info("stopTask fnb request param : {}, {}", params.toString(), params.getData().toString());

        BaseResponseDataModel<ServerStatusModel> responseModel = new BaseResponseDataModel<>();
        ServerStatusModel serverStatusModel = null;

        TaskLog taskLog = daemonService.stopAgentTimer(params.getData().getName());

        if (taskLog != null) {
            if (taskLog.getIsAlive()) {
                responseModel.setResponseCode(CommonConstants.MC_801);
            } else {
                responseModel.setResponseCode(CommonConstants.MC_SUCCESS_BAS);
                serverStatusModel = new ServerStatusModel();
                serverStatusModel.setStopTime(taskLog.getStopDateTime());

                try {
                    // Get last stop time
                    BatchLog paramDomain = new BatchLog();
                    paramDomain.setName(params.getData().getName());
                    paramDomain.setOnOff(CommonConstants.ON_OFF_ON);
                    BatchLog batchLogDomain = batchLogService.selectBatchLog(paramDomain);
                    if (batchLogDomain != null) {
                        serverStatusModel.setStartTime(batchLogDomain.getDateTime());
                    }
                } catch (Exception e) {
                    responseModel.setResponseMessage("stopTask batch log exception: "+ LogUtil.getExceptionLog(e));
                    logger.info("stopTask batch log exception: {}", LogUtil.getExceptionLog(e));
                }
            }
        } else {
            responseModel.setResponseCode(CommonConstants.MC_404);
        }
        responseModel.setData(serverStatusModel);
        return new Gson().toJson(responseModel);
    }

    @PostMapping(value = "/agentstatus")
    public String isAlive(@RequestBody BaseRequestDataModel<BatchLog> params) {
        logger.info("isAlive fnb request param : {}, {}", params.toString(), params.getData().toString());

        BaseResponseDataModel<ServerStatusModel> responseModel = new BaseResponseDataModel<>();
        ServerStatusModel serverStatusModel = new ServerStatusModel();
        responseModel.setResponseCode(CommonConstants.MC_SUCCESS_BAS);

        TaskLog taskLog = daemonService.getTaskLog(params.getData().getName());
        if (taskLog != null) {
            serverStatusModel.setStatus(new Short(CommonConstants.ON_OFF_ON).toString());
            responseModel.setResponseCode(CommonConstants.MC_SUCCESS_BAS);
        } else {
            serverStatusModel.setStatus(new Short(CommonConstants.ON_OFF_OFF).toString());
            responseModel.setResponseCode(CommonConstants.MC_404);
        }

        try {
            BatchLog paramDomain = new BatchLog();
            paramDomain.setName(params.getData().getName());
            paramDomain.setOnOff(CommonConstants.ON_OFF_ON);

            // Get last start time
            BatchLog batchLogDomain = batchLogService.selectBatchLog(paramDomain);
            if (batchLogDomain != null) {
                serverStatusModel.setStartTime(batchLogDomain.getDateTime());
            }
            // Get last stop time
            paramDomain.setOnOff(CommonConstants.ON_OFF_OFF);
            batchLogDomain = batchLogService.selectBatchLog(paramDomain);
            if (batchLogDomain != null) {
                serverStatusModel.setStopTime(batchLogDomain.getDateTime());
            }
        } catch (Exception e) {
            responseModel.setResponseMessage("isAlive batch log exception: "+ LogUtil.getExceptionLog(e));
            logger.info("isAlive batch log exception: {}", LogUtil.getExceptionLog(e));
        }
        responseModel.setData(serverStatusModel);
        return new Gson().toJson(responseModel);
    }

    
}
