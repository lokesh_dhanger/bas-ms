package za.co.hellogroup.bas.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.dao.BatchLog;
import za.co.hellogroup.bas.repository.BatchLogRepository;
import za.co.hellogroup.bas.service.BatchLogService;

@Service("batchLogService")
public class BatchLogServiceImpl implements BatchLogService {

    @Autowired
    private BatchLogRepository batchLogRepository;

    @Override
    public BatchLog selectBatchLog(BatchLog batchLog) throws Exception {
        return batchLogRepository.findByNameOnOffSortedByIdDesc(batchLog.getName(), batchLog.getOnOff());
    }

    @Override
    public BatchLog insertBatchLog(BatchLog batchLog) throws Exception {
        return batchLogRepository.save(batchLog);
    }
}
