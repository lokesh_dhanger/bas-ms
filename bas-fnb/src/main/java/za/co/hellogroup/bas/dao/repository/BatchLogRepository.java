package za.co.hellogroup.bas.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.BatchLog;

@Repository
public interface BatchLogRepository extends JpaRepository<BatchLog, Long> {

}
