package za.co.hellogroup.bas.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AgentTimer extends Timer {
    final static Logger logger = LoggerFactory.getLogger(AgentTimer.class);

	public AgentTimer() {
		super();
	}

	@Override
	public void schedule(TimerTask task, Date startTime, long period) {
		long currentTime = System.currentTimeMillis();
		logger.info("AgentTimer currentTime : {}", currentTime);
		if (startTime.getTime() < currentTime) {
			startTime = getStartTime(currentTime, startTime, period);
		}
		logger.info("AgentTimer : {}, {}", task.scheduledExecutionTime(), task.toString());
		super.schedule(task, startTime, period);
	}

	private Date getStartTime(long currentTime, Date time, long period) {
		Calendar cal = Calendar.getInstance();
		int value = 0;
		int temp = 0;

		long oneSecond = 1000;
		long oneMinute = 60 * oneSecond;
		long oneHour = 60 * oneMinute;
		long oneDay = 24 * oneHour;
		long oneWeek = 7 * oneDay;
		long oneMonth = 30 * oneDay;
		long oneYear = 12 * oneMonth;

		cal.setTime(time);

		if (period >= oneYear) {
			value = truncTime(period / oneYear);
			temp = Calendar.YEAR;
		} else if (period >= oneMonth) {
			value = truncTime(period / oneMonth);
			temp = Calendar.MONTH;
		} else if (period >= oneWeek) {
			value = truncTime(period / oneWeek * 7);
			temp = Calendar.DATE;
		} else if (period >= oneDay) {
			value = truncTime(period / oneDay);
			temp = Calendar.DATE;
		} else if (period >= oneHour) {
			value = truncTime(period / oneHour);
			temp = Calendar.HOUR_OF_DAY;
		} else if (period >= oneMinute) {
			value = truncTime(period / oneMinute);
			temp = Calendar.MINUTE;
		} else if (period >= oneSecond) {
			value = truncTime(period / oneSecond);
			temp = Calendar.SECOND;
		}

		while (currentTime >= cal.getTimeInMillis()) {
			cal.add(temp, value);
		}
		return cal.getTime();
	}

	private int truncTime(long value) {
		if (Integer.MIN_VALUE <= value && Integer.MAX_VALUE >= value) {
			return (int) value;
		}
		return 0;
	}
}
