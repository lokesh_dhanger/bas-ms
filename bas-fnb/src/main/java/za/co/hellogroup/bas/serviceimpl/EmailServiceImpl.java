package za.co.hellogroup.bas.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.dao.EmailDomain;
import za.co.hellogroup.bas.dao.FileDomain;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;
import za.co.hellogroup.bas.service.EmailService;
import za.co.hellogroup.bas.service.SystemPropertyService;
import za.co.hellogroup.bas.util.EmailHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("emailService")
@Scope("prototype")
public class EmailServiceImpl implements EmailService {
    final static Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    SystemPropertyService systemPropertyService;


    /**
     * get timezone with username data
     *
     * @param recipient
     * @param subject
     * @param content
     * @param fileList
     * @return
     */
    public boolean sendEmail(String recipient, String subject, String content, List<FileDomain> fileList) {
//        boolean isSent = false;
        logger.info(">>>>>>> sendEmail : {}, {}, {}", recipient, subject, content);

        List<String> propertyNames = new ArrayList<String>();

        // Get system properties for sending email
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_HOST);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_PORT);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_USER);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_PASSWORD);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_FROM);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_AUTH);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_STARTTLS);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_SSL_TRUST);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_TIMEOUT);
        propertyNames.add(CommonConstants.PROPERTY_NAME_EMAIL_CONNECTIONTIMEOUT);

        Map<String, SystemProperty> propertyItems = systemPropertyService.selectSystemPropertyList(CommonConstants.SYSTEM_CODE_COMMON, propertyNames);
        if (propertyItems != null) {
            logger.info("sendEmail property >>> {}", propertyItems.toString());
            EmailDomain emailDomain = new EmailDomain();
            emailDomain.setHost(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_HOST).getPropertyValue());
            emailDomain.setPort(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_PORT).getPropertyValue());
            emailDomain.setUser(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_USER).getPropertyValue());
            emailDomain.setPassword(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_PASSWORD).getPropertyValue());
            emailDomain.setFrom(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_FROM).getPropertyValue());
            emailDomain.setAuth(Boolean.valueOf(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_AUTH).getPropertyValue()));
            emailDomain.setStartTls(Boolean.valueOf(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_STARTTLS).getPropertyValue()));
            emailDomain.setSslTrust(Boolean.valueOf(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_SSL_TRUST).getPropertyValue()));
            emailDomain.setTimeout(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_TIMEOUT).getPropertyValue());
            emailDomain.setConnectiontimeout(propertyItems.get(CommonConstants.PROPERTY_NAME_EMAIL_CONNECTIONTIMEOUT).getPropertyValue());

            logger.info("sendEmail property EmailDomain >>> {}", emailDomain.toString());

            // send email
            EmailHandler emailHandler = new EmailHandler();
            emailHandler.setEmailConfig(emailDomain, recipient, subject, content, fileList);
            Thread emailThread = new Thread(emailHandler);
            emailThread.start();
        } else {
            logger.info("sendEmail : Not Found system property >>> {}, {}", CommonConstants.SYSTEM_CODE_COMMON, Arrays.toString(propertyNames.toArray()));
        }

//			timezone = mapper.getTimezoneByUsername(username);
        return true;
    }
}