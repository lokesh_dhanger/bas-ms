package za.co.hellogroup.bas.dao;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@ToString
@Entity
@Table(name = "code_detail")
public class CodeDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8526143462597605938L;

	@Column(name = "group_code")
	private String groupCode;

	@Column(name = "item_code")
	private String itemCode;

	@Column(name = "item_name")
	private String itemName;

	@Column(name = "on_off")
	private transient int onOff;

	@Column(name = "display_order")
	private int displayOrder;

	@Column(name = "description")
	private transient String description;

	@Column(name = "created_on")
	@CreationTimestamp
	private Date createdOn;

	@Column(name = "updated_on")
	@UpdateTimestamp
	private Date updatedOn;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_by")
	private String updatedBy;

	@PreUpdate
	protected void onUpdate() {
		this.updatedOn = new Date();
	}

	@PrePersist
	protected void onCreate() {
		this.createdOn = new Date();
		this.updatedOn = new Date();
	}

}