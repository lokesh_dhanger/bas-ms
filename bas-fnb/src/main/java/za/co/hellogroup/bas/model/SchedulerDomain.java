package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
public class SchedulerDomain {

    private long id;
    private String name;
    private String targetProgram;
    private String planText;
    private String seconds;
    private String minutes;
    private String hours;
    private String dom;
    private String month;
    private String dow;
    private String year;
    private String onOff;
    private String description;
    protected String createdBy;
    protected String createdOn;
    protected String updatedBy;
    protected String updatedOn;


    public String getCronExpression() {
        StringBuffer sb = new StringBuffer();
        
        if (StringUtils.isNotBlank(getSeconds())) {
            sb.append(getSeconds());
            sb.append(" ");
        }
        sb.append(getMinutes());
        sb.append(" ");
        sb.append(getHours());
        sb.append(" ");
        sb.append(getDom());
        sb.append(" ");
        sb.append(getMonth());
        sb.append(" ");
        sb.append(getDow());
        sb.append(" ");
        if (StringUtils.isNotBlank(getYear())) {
            sb.append(getYear());
        }
        return sb.toString();
    }
}
