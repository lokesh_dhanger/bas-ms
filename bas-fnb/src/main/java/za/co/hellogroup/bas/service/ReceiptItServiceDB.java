package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.dao.inoutlog.InOutLog;


public interface ReceiptItServiceDB {
    public int insertInOutLog(InOutLog domain) throws Exception;
    public int updateInOutLog(InOutLog domain) throws Exception;
}
