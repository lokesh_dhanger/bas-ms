package za.co.hellogroup.bas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.BatchLog;

@Repository
public interface BatchLogRepository extends JpaRepository<BatchLog, Long> {

    @Query("SELECT * FROM batch_log where name=:name AND on_off=:onOff ORDER BY id DESC LIMIT 1")
    BatchLog findByNameOnOffSortedByIdDesc(String name, int onOff);

}
