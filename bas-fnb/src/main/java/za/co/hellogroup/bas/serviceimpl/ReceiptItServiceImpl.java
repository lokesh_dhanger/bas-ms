package za.co.hellogroup.bas.serviceimpl;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.dao.inoutlog.InOutLog;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;
import za.co.hellogroup.bas.enums.BasType;
import za.co.hellogroup.bas.enums.StatusCode;
import za.co.hellogroup.bas.model.BaseRequestDataModel;
import za.co.hellogroup.bas.model.BaseResponseModel;
import za.co.hellogroup.bas.model.TransactionMasterModel;
import za.co.hellogroup.bas.service.CommonService;
import za.co.hellogroup.bas.service.InOutLogService;
import za.co.hellogroup.bas.service.ReceiptItService;
import za.co.hellogroup.bas.service.SystemPropertyService;
import za.co.hellogroup.bas.util.DataParser;
import za.co.hellogroup.bas.util.DateTime;
import za.co.hellogroup.bas.util.HttpUtil;
import za.co.hellogroup.bas.util.LogUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("receiptItService")
@Scope("prototype")
public class ReceiptItServiceImpl implements ReceiptItService {

    final static Logger logger = LoggerFactory.getLogger(ReceiptItServiceImpl.class);

	@Autowired
	private InOutLogService inOutLogService;

    @Autowired
	private CommonService commonService;

    @Autowired
	private SystemPropertyService systemPropertyService;

    public boolean transferTransactionData(String msgText) throws Exception {
        logger.info("Starting transferTransactionData : {}", msgText);
        boolean isSuccess = false;
        //int cnt = 0;
       	InOutLog logDomain = new InOutLog();
       	logDomain.setBasType(BasType.FNB.getValue());
       	logDomain.setRequestText(msgText);
       	logDomain.setStatusCode(StatusCode.READY.getValue());
       	logDomain.setCreatedBy(CommonConstants.SYS_USERNAME);
       	logDomain.setCreatedOn(DateTime.getPastDate(new Date(), 0));
       	logDomain.setUpdatedBy(logDomain.getCreatedBy());
       	logDomain.setUpdatedOn(logDomain.getCreatedOn());

        try {
        	//// 1. Pre-task
        	// 1-1. Insert request data into a log table (Bank)
			//cnt = inOutLogService.insertInOutLog(logDomain);
			logDomain = inOutLogService.insertInOutLog(logDomain);
        	
       		// 1-2. Parsing transaction data from bank
   		    DataParser dp = new DataParser();
   		    TransactionMasterModel transactionModel = dp.parseBankTransactionData(msgText);

        	if (transactionModel == null) {
		    	logDomain.setStatusCode(StatusCode.DROP.getValue());
       		    logDomain.setStatusText("Invalid bank data format : "+ msgText);
   		    } else {
        		logDomain.setRefNumber(transactionModel.getRefNumber());
        		logDomain.setChannel(transactionModel.getChannel());
        		logDomain.setTransactionId(transactionModel.getTransactionId());
        		logDomain.setTransactionAmount(transactionModel.getTransactionAmount());
        		logDomain.setTransactionDate(transactionModel.getTransactionDate());
        		logDomain.setDebitCredit(transactionModel.getDebitCredit());
        		logDomain.setTransactionTypeMain(transactionModel.getTransactionTypeMain());
        		logDomain.setTransactionTypeSub(transactionModel.getTransactionTypeSub());
        		logDomain.setIndicator(transactionModel.getIndicator());

   		    	logger.info("transactionMasterModel : {}", transactionModel);
        		// 1-3. Validate reference number
        		boolean isValidRefNumber = commonService.validateRefNumber(transactionModel.getRefNumber());
        		if (isValidRefNumber) {
        			// 1-4. Get system properties from database
        			List<String> propertyKeyList = new ArrayList<String>();
        			propertyKeyList.add(CommonConstants.BAS_USERNAME);
        			propertyKeyList.add(CommonConstants.BAS_PASSWORD);
        			propertyKeyList.add(CommonConstants.BAS_URL_FOR_TRANSACTION);
        			Map<String, SystemProperty> propertyKeyMap = systemPropertyService.selectSystemPropertyList(CommonConstants.SYSTEM_CODE_COMMON, propertyKeyList);

            	    if (propertyKeyMap != null && propertyKeyMap.size() == propertyKeyList.size()) {
            	    	//// 2. Biz process
        			    // 2-1. Set authentication and transaction data
            	    	BaseRequestDataModel<TransactionMasterModel> dataModel = new BaseRequestDataModel<TransactionMasterModel>();
            	    	dataModel.setUsername(propertyKeyMap.get(CommonConstants.BAS_USERNAME).getPropertyValue());
            	    	dataModel.setPassword(propertyKeyMap.get(CommonConstants.BAS_PASSWORD).getPropertyValue());
            	    	transactionModel.setId(logDomain.getId());
            	    	dataModel.setData(transactionModel);
            	    	
        			    // 2-2. Send transaction data to BAS core
        			    String responseData = HttpUtil.sendPostJson(propertyKeyMap.get(CommonConstants.BAS_URL_FOR_TRANSACTION).getPropertyValue(), dataModel);
        		    
        			    if (responseData != null) {
        			    	// 2-4. Parsing response data
        			    	try {
        			    		Gson gson = new Gson();
        			    		BaseResponseModel responseModel = gson.fromJson(responseData, BaseResponseModel.class);
        			    		
        			    		if (responseModel != null) {
        			    			if (responseModel.getResponseCode().equals(CommonConstants.MC_SUCCESS_BAS)) {
        			    				logDomain.setStatusCode(StatusCode.SENT.getValue());
        			    			} else {
        			    				logDomain.setStatusCode(StatusCode.FAIL.getValue());
        			    			}
        			    			logDomain.setMessageCode(responseModel.getResponseCode());
        			    			logDomain.setMessageText(responseModel.getResponseMessage());
        			    		} else {
       			    				logDomain.setStatusCode(StatusCode.FAIL.getValue());
       			    				logDomain.setStatusText("[Double check] Failed to get response from BAS, so please double check if BAS got this data or not");
        			    		}
        			    		isSuccess = true;
        			    	} catch (Exception e) {
        			    		logDomain.setStatusCode(StatusCode.FAIL.getValue());
        			    		logDomain.setStatusText("[Double check] Failed to parse response data from BAS core : "+ responseData);
        			    	}
        			    } else {
       			    		logDomain.setStatusCode(StatusCode.FAIL.getValue());
       			    		logDomain.setStatusText("[Double check] Failed to get response data from BAS");
        			    }
            	    } else {
        			    logDomain.setStatusCode(StatusCode.FAIL.getValue());
        			    logDomain.setStatusText("Could not get system property data. Please check these system property data : "+ CommonConstants.BAS_USERNAME +", "+ CommonConstants.BAS_PASSWORD +", "+ CommonConstants.BAS_URL_FOR_TRANSACTION);
            	    }
        		} else {
        			logDomain.setStatusCode(StatusCode.FAIL.getValue());
       			    logDomain.setStatusText("Invalid reference number : "+ transactionModel.getRefNumber());
        		}
   		    }
        } catch (Exception e) {
            logger.error("transferTransactionData error : {}", LogUtil.getExceptionLog(e));
        } finally {
        	//if (cnt > 0) {
        		// 1-4. Update request data into a log table (Bank)
        		logDomain.setUpdatedOn(DateTime.getPastDate(new Date(), 0));
				inOutLogService.updateInOutLog(logDomain);
        	//}
        	logger.info("Ending transferTransactionData......");
        }

        return isSuccess;
    }
}