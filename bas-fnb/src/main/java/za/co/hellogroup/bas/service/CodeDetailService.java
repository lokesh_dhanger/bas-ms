package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.dao.BaseMessage;
import za.co.hellogroup.bas.dao.CodeDetail;

import java.util.List;

public interface CodeDetailService {

	public List<CodeDetail> selectCodeList(String groupCode);
	public CodeDetail selectCode(String groupCode, String itemCode);
	public BaseMessage generateResponseMessage(String groupCode, String itemCode);

}