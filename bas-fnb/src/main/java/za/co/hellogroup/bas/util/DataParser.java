package za.co.hellogroup.bas.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.enums.*;
import za.co.hellogroup.bas.model.TransactionMasterModel;

import java.math.BigDecimal;

public class DataParser {
    final static Logger logger = LoggerFactory.getLogger(DataParser.class);

    /**
     * Parse transaction data
     * 
     * @param msgText message text from bank
     * @return parsed data object
     */
	public TransactionMasterModel parseBankTransactionData (String msgText) {
		int nextIdx = 0;
	    TransactionMasterModel model = null;
	    String indicatorAdjustment = null;
	    String indicatorReversal = null;

	    try {
	    	model = new TransactionMasterModel();

	    	model.setBasType(BasType.FNB.getValue());
		   	model.setCreatedBy(CommonConstants.SYS_USERNAME);
		   	model.setCreatedOn(DateTime.getLocalDateTime());

		   	String tmp = null;

		   	model.setTransactionId(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B01_CORRELATION_IDENTIFIER.getValue()).trim());
//System.out.println("1 > "+ nextIdx +","+ model.getTransactionId());
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B02_ACCOUNT_NUMBER.getValue()).trim();
//System.out.println("2 > "+ nextIdx +","+ tmp);
		   	model.setDebitCredit(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B03_DEBIT_CREDIT_INDICATOR.getValue()).trim().toUpperCase());
//System.out.println("3 > "+ nextIdx +","+ model.getDebitCredit());
		   	model.setTransactionTypeMain(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B04_TRANSACTION_CATEGORY_CODE.getValue()).trim());
//System.out.println("4 > "+ nextIdx +","+ model.getTransactionTypeMain());
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B05_TRANSACTION_CATEGORY_NAME.getValue()).trim();
//System.out.println("5 > "+ nextIdx +","+ tmp);
		   	model.setTransactionTypeSub(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B06_TRANSACTION_SUB_CATEGORY_CODE.getValue()).trim());
//System.out.println("6 > "+ nextIdx +","+ model.getTransactionTypeSub());
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B07_TRANSACTION_SUB_CATEGORY_NAME.getValue()).trim();
//System.out.println("7 > "+ nextIdx +","+ tmp);
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B08_RETURN_REASON_CODE.getValue()).trim();
//System.out.println("8 > "+ nextIdx +","+ tmp);
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B09_RETURN_REASON_DESCRIPTION.getValue()).trim();
//System.out.println("9 > "+ nextIdx +","+ tmp);
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B10_EFFECTIVE_DATE.getValue()).trim();
//System.out.println("10 > "+ nextIdx +","+ tmp);
		   	CurrencyConverter cc = new CurrencyConverter();
			cc.setScale(2);
		   	model.setTransactionAmount(cc.getDivide(new BigDecimal(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B11_TRANSACTION_AMOUNT.getValue()).trim()), new BigDecimal(100)));
//System.out.println("11 > "+ nextIdx +","+ model.getTransactionAmount());
		   	model.setRefNumber(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B12_REFERENCE.getValue()).trim());
//		   	if (StringUtils.isNoneBlank(model.getRefNumber()) && model.getRefNumber().length() > Constants.REF_NUMBER_LENGTH) {
//		   		model.setRefNumber(model.getRefNumber().substring(model.getRefNumber().length() - Constants.REF_NUMBER_LENGTH));
//		   	}
//System.out.println("12 > "+ nextIdx +","+ model.getRefNumber());
//		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B13_POST_DATE.getValue()).trim();
		   	model.setTransactionDate(DateTime.convertDateTimeToString(DateTime.getDateFromFormat(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B13_POST_DATE.getValue()).trim(), CommonConstants.FORMAT_DATE_TIME_YMD)));
//System.out.println("13 > "+ nextIdx +","+ model.getTransactionDate());
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B14_TRACE_IDENTIFIER.getValue()).trim();
//System.out.println("14 > "+ nextIdx +","+ tmp);
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B15_CALENDAR_DATE_TIME.getValue()).trim();
//		   	model.setTransactionDate(DateTime.convertDateTimeToString(DateTime.getDateFromFormat(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B15_CALENDAR_DATE_TIME.getValue()).trim(), Constants.FORMAT_DATE_TIME_YMDHMSS)));
//System.out.println("15 > "+ nextIdx +","+ tmp);
		   	model.setChannel(msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B16_CHANNEL.getValue()).trim());
//System.out.println("16 > "+ nextIdx +","+ model.getChannel());
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B17_BRANCH_LOCATION.getValue()).trim();
//System.out.println("17 > "+ nextIdx +","+ tmp);
		   	tmp = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B18_OPERATING_LIMIT_INDICATOR.getValue()).trim();
//System.out.println("18 > "+ nextIdx +","+ tmp);
		   	indicatorAdjustment = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B19_ADJUSTMENT_INDICATOR.getValue()).trim();
//System.out.println("19 > "+ nextIdx +","+ indicatorAdjustment);
		   	indicatorReversal = msgText.substring(nextIdx, nextIdx += MsgBodyNotification.B20_REVERSAL_INDICATOR.getValue()).trim();
//System.out.println("20 > "+ nextIdx +","+ indicatorReversal);

			model.setIndicator(CommonConstants.INDICATOR_ANY);
			if ("N".equals(indicatorAdjustment) && "N".equals(indicatorReversal)) {
				if (CommonConstants.DEBIT_CREDIT_C.equals(model.getDebitCredit())) {
					model.setIndicator(TransactionTypeGroup.CREDIT.getValue());
				} else if (CommonConstants.DEBIT_CREDIT_D.equals(model.getDebitCredit())) {
					model.setIndicator(TransactionTypeGroup.DEBIT.getValue());
				} else {
					model.setIndicator("X");  // review case
				}
			} else if ("N".equals(indicatorAdjustment) && "Y".equals(indicatorReversal)) {
				model.setIndicator(TransactionTypeGroup.REVERSAL.getValue());
			} else if ("Y".equals(indicatorAdjustment) && "N".equals(indicatorReversal)) {
				model.setIndicator(TransactionTypeGroup.ADJUSTMENT.getValue());
			}
	    } catch (Exception e) {
	    	model = null;
	    	logger.error("parseBankTransactionData error : {}", LogUtil.getExceptionLog(e));
	    }

		return model;
	}

	public TransactionMasterModel parseBankTransactionDataBulk (String msgText) {
	    TransactionMasterModel model = null;
		int nextIdx = 0;

		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H01_STMT_COMPANY.getValue()).trim();
		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H02_STMT_PRODUCT.getValue()).trim();
		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H03_STMT_BRANCH.getValue()).trim();
		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H04_STMT_ACC_NO.getValue()).trim();
		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H05_STMT_PROC_DATE.getValue()).trim();
		msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H06_STMT_REC_SEQ.getValue()).trim();

		String msgType = msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H07_STMT_REC_TYP.getValue()).trim();
		
		if (msgType != null) {
			if (CommonConstants.MSG_HEADER.equals(msgType)) {
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H08_STMT_ACCT_TYPE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H09_STMT_STMT_DATE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H10_STMT_PREV_DATE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H11_STMT_PREV_BAL.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H12_STMT_PREV_BAL_DC.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H13_STMT_CURR_BAL.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H14_STMT_CURR_BAL_DC.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H15_STMT_TOT_CR_NO.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H16_STMT_TOT_CR_AMT.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H17_STMT_TOT_DR_NO.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H18_STMT_TOT_DR_AMT.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H19_STMT_STMT_NO.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H20_STMT_SF_IND.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H21_STMT_SF_FLAT.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H22_STMT_SF_MIN.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H23_STMT_SF_PER.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H24_STMT_SF_MAX.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H25_STMT_ANN_RATE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H26_STMT_CUST_NAME.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H27_FILLER1.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H28_STMT_VAT_AMT.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H29_STMT_LANG_DESC.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H30_FILLER2.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgHeaderBulk.H31_STMT_VERSION.getValue()).trim();
			} else if (CommonConstants.MSG_BODY.equals(msgType)) {
				model = new TransactionMasterModel();
				model.setBasType(BasType.FNB.getValue());
				model.setTransactionId("");
				model.setCreatedBy("");
				model.setCreatedOn(DateTime.getLocalDateTime());

				model.setTransactionDate(msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B08_STMT_EFF_DATE.getValue()).trim());
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B09_STMT_TX_POST_DATE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B10_STMT_TRAN_CODE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B11_STMT_RULE.getValue()).trim();
				model.setTransactionAmount(new BigDecimal(msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B12_STMT_AMOUNT.getValue()).trim()));
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B13_STMT_DR_CR_IND.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B14_STMT_REV_IND.getValue()).trim();
				model.setTransactionTypeMain(msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B15_STMT_CATEGORY.getValue()).trim());
				model.setTransactionTypeSub(msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B16_STMT_SUB_CATEGORY.getValue()).trim());
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B17_STMT_SRC_SYSTEM.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B18_STMT_TXN_BRANCH.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B19_STMT_ID_NO.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B20_STMT_SERV_FEE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B21_STMT_SERV_DC.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B22_STMT_CK_SER_NO.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B23_STMT_TXN_NARRATIVE.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B24_RESERVED.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B25_STMT_CLC_BRANCH.getValue()).trim();
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B26_STMT_TRAN_TYPE.getValue()).trim();
				model.setRefNumber(msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B27_STMT_REFERENCE.getValue()).trim());
				msgText.substring(nextIdx, nextIdx += MsgBodyBulk.B28_STMT_VERSION.getValue()).trim();
			}
		}
		
		return model;
	}
}