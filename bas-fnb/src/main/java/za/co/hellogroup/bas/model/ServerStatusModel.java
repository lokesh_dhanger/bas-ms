package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

@Getter
@Setter
@ToString
public class ServerStatusModel {

	private String startTime;
    private String stopTime;
    private String status;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ServerStatusModel that = (ServerStatusModel) o;
		return Objects.equals(startTime, that.startTime) && Objects.equals(stopTime, that.stopTime) && Objects.equals(status, that.status);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startTime, stopTime, status);
	}

}