package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Getter
@Setter
@ToString
public class TaskLog implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3076161111536399562L;
	private long seqNo;
	private Boolean isAlive;
	private String startDateTime;
	private String stopDateTime;
	private String taskName;
	private String taskMessage;

	public void clear() {
		this.seqNo = 0L;
		this.isAlive = false;
		this.startDateTime = null;
		this.stopDateTime = null;
		this.taskName = null;
		this.taskMessage = null;
	}

	public TaskLog() {
		clear();
	}


}