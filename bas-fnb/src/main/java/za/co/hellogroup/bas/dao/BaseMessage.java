package za.co.hellogroup.bas.dao;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class BaseMessage implements Serializable {

	private String messageCode;
    private String messageText;
}