package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.model.TaskLog;

public interface DaemonService {

    public void startAgentTimer(String taskName);

    public TaskLog stopAgentTimer(String taskName);

    public boolean isAlive(String taskName);

    public TaskLog getTaskLog(String taskName);
}
