package za.co.hellogroup.bas.dao;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class FileDomain implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5018421141964656165L;

	private long id;
	private String fileNameOrigin;
	private String fileNameNew;
	private String fileExt;
	private String fileSize;
	private String description;
	private byte[] fileContent;
	private int fileType;
	private String foreignCreatedOn;
	private String foreignUpdatedOn;
	protected String createdBy;
	protected String createdOn;
	protected String updatedBy;
	protected String updatedOn;

}