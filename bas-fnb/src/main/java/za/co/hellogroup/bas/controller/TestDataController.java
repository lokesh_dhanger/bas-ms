package za.co.hellogroup.bas.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.hellogroup.bas.model.BaseRequestDataModel;
import za.co.hellogroup.bas.service.ReceiptItService;

@RestController
@RequestMapping("fnb")
public class TestDataController {

	final static Logger logger = LoggerFactory.getLogger(TestDataController.class);
    
    @Autowired
	ReceiptItService receiptItService;


	@PostMapping(value = "/createtransaction")
	public String createTransaction(@RequestBody BaseRequestDataModel<String> params) throws Exception {
		logger.info("createTransaction fnb request param : {}, {}", params.toString(), params.getData());

		if (params != null
				&&  StringUtils.isNoneBlank(params.getData())
				&&  receiptItService.transferTransactionData(params.getData())) {
			return "Success";
		} else {
			return "Fail";
		}
	}
}