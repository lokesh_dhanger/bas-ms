package za.co.hellogroup.bas.serviceimpl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.model.TaskLog;
import za.co.hellogroup.bas.service.DaemonService;
import za.co.hellogroup.bas.task.AgentTimer;
import za.co.hellogroup.bas.task.AgentTimerTask;
import za.co.hellogroup.bas.task.Job;
import za.co.hellogroup.bas.util.LogUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("daemonService")
public class DaemonServiceImpl implements DaemonService {

    final static Logger logger = LoggerFactory.getLogger(DaemonService.class);

    private static List<AgentTimerTask> agentTimerTaskList = null;

    @Override
    public void startAgentTimer(String taskName) {
        // Remove all tasks if there are.
        removeAllTaskList();

		/*
		try {
			Thread.sleep(10000);
		} catch (Exception e) {

		}
		*/
        // Create tasks
        generateTaskList(taskName);

        try {
            logger.info("agentTimerTaskList size : {}", agentTimerTaskList.size());

            List<AgentTimer> agentTimerList = new ArrayList<AgentTimer>();
            AgentTimer agentTimer = null;
            for (AgentTimerTask agentTimerTask : agentTimerTaskList) {
                logger.info("startAgentTimer...... {}, {}", agentTimerTask.getTaskName(), agentTimerTask.getStartTime());

                agentTimer = new AgentTimer();
//				agentTimer.schedule(agentTimerTask, agentTimerTask.getFirstTime(), agentTimerTask.getPeriod());
                agentTimer.schedule(agentTimerTask, agentTimerTask.getStartTime());  // run timer
                agentTimerList.add(agentTimer);
            }
            logger.info("startAgentTimer out for loop");
        } catch (Exception e) {
            logger.info("startAgentTimer error : {}", LogUtil.getExceptionLog(e));
        } finally {
            logger.info("startAgentTimer finished");
        }
    }

    @Override
    public TaskLog stopAgentTimer(String taskName) {
        return null;
    }

    @Override
    public boolean isAlive(String taskName) {
        boolean taskStatus = false;

        if (agentTimerTaskList != null && StringUtils.isNoneBlank(taskName)) {
            Job job = null;
            MessageClientService taskService = null;
            int jobCount = 0;
            for (AgentTimerTask agentTimerTask : agentTimerTaskList) {
                jobCount = agentTimerTask.size();
                for (int j = 0; j < jobCount; j++) {
                    job = agentTimerTask.getElement(j);
                    if (job != null && job.getTaskName().equals(taskName.toUpperCase())) {
                        taskService = (MessageClientService)job.getJob();
                        taskStatus = taskService.getStatus();
                        break;
                    }
                }
            }
        }
        return taskStatus;
    }


    /**
     * Get task log of a specific task
     */
    @Override
    public TaskLog getTaskLog(String taskName) {
        TaskLog taskLog = null;
        int maxLoopCount = 100;

        if (agentTimerTaskList != null && StringUtils.isNoneBlank(taskName)) {
            Job job = null;
            MessageClientService taskService = null;
            int jobCount = 0;
            int loopCount = 0;
            for (AgentTimerTask agentTimerTask : agentTimerTaskList) {
                jobCount = agentTimerTask.size();
                for (int j = 0; j < jobCount; j++) {
                    job = agentTimerTask.getElement(j);
                    if (job != null && job.getTaskName().equals(taskName.toUpperCase())) {
                        taskService = (MessageClientService)job.getJob();
                        if (taskService != null) {
                            loopCount = 0;
                            while (true) {
                                taskLog = taskService.getTaskLog();
                                if (taskLog == null) {
                                    try {
                                        Thread.sleep(300);
                                        if (++loopCount > maxLoopCount) {
                                            break;
                                        }
                                    } catch (Exception e) {}
                                } else {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return taskLog;
    }

    /**
     * Currently we have only one Job
     * @return
     */
    private static void generateTaskList(String taskName) {
        logger.info("generateTaskList started... : {}", taskName);
        try {
            agentTimerTaskList = new ArrayList<AgentTimerTask>();

            //// Task Group1
            AgentTimerTask agentTimerTask = null;
            agentTimerTask = new AgentTimerTask();
            agentTimerTask.setTaskName("BAS_TASK_GROUP");
            agentTimerTask.setStartTime(new Date());
//			agentTimerTask.setPeriod(1000 * 60);
            // Task1 definition of group1: start
            // BAS Core (between BAS and HP)
            agentTimerTask.addElement(new Job(new MessageClientService(), "startTaskService", taskName.toUpperCase(), false));
            // Task1 definition of group1: end

            agentTimerTaskList.add(agentTimerTask);
        } catch (Exception e) {
            logger.info("generateTaskList error : {}", LogUtil.getExceptionLog(e));
        }
    }

    /**
     * If there are still running tasks, stop all tasks and remove the list.
     * @return
     */
    private static void removeAllTaskList() {
        if (agentTimerTaskList != null) {
            Job job = null;
            MessageClientService taskService = null;
            int jobCount = 0;

            try {
                for (AgentTimerTask agentTimerTask : agentTimerTaskList) {
                    jobCount = agentTimerTask.size();
                    for (int j = 0; j < jobCount; j++) {
                        job = agentTimerTask.getElement(j);
                        if (job != null) {
                            taskService = (MessageClientService)job.getJob();
                            taskService.stop();
                            break;
                        }
                    }
                    if (jobCount > 0) {
                        agentTimerTask.clear();
                    }
                }
                agentTimerTaskList.clear();
            } catch (Exception e) {
                logger.info("removeAllTaskList error : {}", LogUtil.getExceptionLog(e));
            }
        }
    }
}
