package za.co.hellogroup.bas.serviceimpl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.fnb.receiptit.*;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.dao.BatchLog;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;
import za.co.hellogroup.bas.exception.StopException;
import za.co.hellogroup.bas.model.TaskLog;
import za.co.hellogroup.bas.service.BatchLogService;
import za.co.hellogroup.bas.service.EmailService;
import za.co.hellogroup.bas.service.ReceiptItService;
import za.co.hellogroup.bas.service.SystemPropertyService;
import za.co.hellogroup.bas.util.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("messageClientService")
public class MessageClientService {

	final static Logger logger = LoggerFactory.getLogger(MessageClientService.class);

	@Autowired
	private BatchLogService batchLogService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SystemPropertyService systemPropertyService;

	@Autowired
	private ReceiptItService receiptItService;
    
	private TaskLog taskLog = null;
    private boolean isStop = false;
    private boolean stoppedByAdmin = false;
    private static int INTERVAL_NEXT_PING = 3000;
    private static int MAX_WAITING_TIME = 600;  // about 30 minutes (600), interval second is 3


	/**
	 *
	 * @param taskName
	 */
	public void startTaskService (String taskName) {
		if (!stoppedByAdmin) {
			taskLog = new TaskLog();
			taskLog.setIsAlive(true);
			taskLog.setTaskName(taskName);
			taskLog.setStartDateTime(DateTime.getDateTime());
		}

//		Thread.currentThread().setName("MessageClient");

		int waitingCount = 0;
		String fnbPropertyFilePath = null;
		MessageConnection connection = null;

		try {
//			ClassLoader classLoader = getClass().getClassLoader();
//			logger.info("Creating new connection1: {}", classLoader.getResource("conf/ReceiptIt.properties").getPath().toString());
//			logger.info("Creating new connection2: {}", Thread.currentThread().getContextClassLoader().getResource("conf/ReceiptIt.properties").getPath().toString());
			logger.info("Creating new connection : {}", fnbPropertyFilePath);

			// Create the message factory
			MessageFactory messageFactory = new MessageFactory();
			// Creates a new connection
			connection = ConnectionFactory.getMessageConnection(CommonConstants.FNB_ROOT_PATH +"/conf/ReceiptIt.properties", messageFactory);

			logger.info("Connecting to remote host");
			// Connect to the message server
			connection.connect();

			// Create and send a new ping message.
			Ping ping = messageFactory.createPing();
			connection.send(ping);

			// Send request to server
//			MessageInterest[] interests = { MessageInterest.NOTIFICATION, MessageInterest.RESEND };
			MessageInterest[] interests = { MessageInterest.NOTIFICATION };
			RegisterInterestRequest rireq = messageFactory.createRegisterInterestRequest(interests);
			connection.send(rireq);

			/*
			// Create start and end date objects for requests.
			Date startDate = null;
			Date endDate = null;
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmm");
			try {
				startDate = fmt.parse("200712011700");
				endDate = fmt.parse("201605032359");
			} catch (ParseException e) {
				logger.info("MessageClient error :"+ e.getMessage());
				e.printStackTrace();
			}

			// Creates a resend request.
			// Please take note of the restrictions in the client brief.
			ResendRequest rsreq = messageFactory.createResendRequest(startDate, endDate);
			connection.send(rsreq);

			// Creates a download request.
			// Please take note of the restrictions in the client brief.
			// The product account number and post dates are optional and not used here.
			DownloadRequest dlreq = messageFactory.createDownloadRequest(startDate, endDate, null, null);
			connection.send(dlreq);
			*/

			long diffTime = 0L;
			isStop = false;
//			String token = null;

			Message msg = null;
//			ReceiptItService receiptItService = new ReceiptItServiceImpl();
			ResendResponse resendResponse = (ResendResponse) msg;
			RegisterInterestResponse res = null;
			DownloadResponse downloadResponse = null;
			Notification notification = null;
			while (!isStop) {
				if (waitingCount >= MAX_WAITING_TIME) {
					logger.error("waitingCount exceed: {}", waitingCount);
					throw new StopException();
				}

				// Check if a new message is available
				msg = connection.poll();

				if (msg == null) {
					waitingCount++;
					logger.info("Waiting for next transactoin...");
				} else if (msg instanceof Pong) {
					waitingCount++;
					diffTime = System.currentTimeMillis() - ((Pong) msg).getPingTime();
					logger.info("Ping to host took " + diffTime + " milliseconds");
				} else if (msg instanceof RegisterInterestResponse) {
					waitingCount++;
					res = (RegisterInterestResponse) msg;
					logger.info("Registered interest [returnCode="+ res.getReturnCode().name() + "] [message="+ res.getMessage() + "]");
					if (res.getReturnCode() != RegisterInterestResponse.ReturnCode.SUCCESS) {
						// Could not register interest in the message types; typically because another instance of the client has interest.
						// You will not receive any messages.
					}
				} else if (msg instanceof Notification) {
					waitingCount = 0;
					notification = (Notification) msg;
					logger.info(">>>>>>>>>>>> Received notification data from FNB:{}", notification.toString());

					receiptItService.transferTransactionData(notification.toString());

					// Process the message here.
					// If there is an error processing the message, you should _not_ acknowledge the message.
					// Terminate the application and fix the problem.
					// You will not be able to get any further messages if you do not ack the message.
					// Tel the server that we have processed the notification successfully.

					notification.acknowledge();
				} else if (msg instanceof ResendResponse) {
					waitingCount++;
					resendResponse = (ResendResponse) msg;
					logger.info("Resend response : returnCode={}, message={}, requestId={}", res.getReturnCode().name(), res.getMessage(), resendResponse.getRequestId());
				} else if (msg instanceof DownloadResponse) {
					waitingCount++;
					downloadResponse = (DownloadResponse) msg;
					logger.info("Download response : returnCode={}, message={}, requestId={}", res.getReturnCode().name(), res.getMessage(), downloadResponse.getRequestId());
				} else {
					logger.error("Unhandled message : {}", msg.toString());
					throw new StopException();
				}
				Thread.sleep(INTERVAL_NEXT_PING);
			}
			logger.info(">>> in MessageClientService out while loop");
		} catch (StopException se) {
			logger.error("StopException error");
		} catch (Exception e) {
			logger.error("MessageClientService error : {}", e);
		} finally {
			isStop = true;
			if (connection != null) {
				logger.info("Disconnecting from remote host");
				connection.disconnect();
			}
			
			List<String> propertyKeyList = new ArrayList<String>();
			propertyKeyList.add(CommonConstants.SA_EMAIL);
  			propertyKeyList.add(CommonConstants.PROPERTY_NAME_MAIN_DOMAIN);

//			SystemPropertyServiceDB systemPropertyServiceDB = ApplicationContextHolder.getContext().getBean(SystemPropertyServiceDB.class);
			Map<String, SystemProperty> propertyKeyMap = systemPropertyService.selectSystemPropertyList(CommonConstants.SYSTEM_CODE_COMMON, propertyKeyList);
//			EmailService emailService = ApplicationContextHolder.getContext().getBean(EmailService.class);
			if (propertyKeyMap != null
			&&  propertyKeyList.size() == propertyKeyMap.size()
			&&	StringUtils.isNotBlank(propertyKeyMap.get(CommonConstants.SA_EMAIL).getPropertyValue())) {
				if (stoppedByAdmin) {
					emailService.sendEmail(propertyKeyMap.get(CommonConstants.SA_EMAIL).getPropertyValue(), "FNB Server down by admin", propertyKeyMap.get(CommonConstants.PROPERTY_NAME_MAIN_DOMAIN).getPropertyValue() +" : "+ DateTime.getLocalDateTime(), null);
				} else {
					emailService.sendEmail(propertyKeyMap.get(CommonConstants.SA_EMAIL).getPropertyValue(), "FNB Server down by other issues", propertyKeyMap.get(CommonConstants.PROPERTY_NAME_MAIN_DOMAIN).getPropertyValue() +" : "+ DateTime.getLocalDateTime(), null);
				}
			} else {
				logger.info("startTaskService stopped, but could not send email");
			}

			if (stoppedByAdmin) {
				taskLog.setIsAlive(false);;
				taskLog.setStopDateTime(DateTime.getDateTime());
				logger.info("MessageClientService ended...");

				try {
					//BatchLogServiceDB batchLogServiceDB = ApplicationContextHolder.getContext().getBean(BatchLogServiceDB.class);

					// Insert batch log
					BatchLog paramDomain = new BatchLog();
					paramDomain.setName(taskName);
					paramDomain.setOnOff(CommonConstants.ON_OFF_OFF);
					paramDomain.setDateTime(taskLog.getStopDateTime());
					batchLogService.insertBatchLog(paramDomain);
				} catch (Exception e) {
					logger.info("startTaskService : failed to log");
				}
			} else {  // If this process stopped by system or network reason, start it again
				if (waitingCount <= 0) {
					logger.info("startTaskService will be waiting for next server restarting...{}", INTERVAL_NEXT_PING * MAX_WAITING_TIME);
					try {
						Thread.sleep(INTERVAL_NEXT_PING * MAX_WAITING_TIME);
					} catch (Exception e) {}
				}
				logger.info("startTaskService is restarting...");
				startTaskService (taskName);
			}
		}
	}
	
	public TaskLog getTaskLog() {
		return taskLog;
	}

	/**
	 * Stop this process
	 * It is called by outside
	 */
	public void stop() {
//		this.isStop = !taskLog.getIsAlive();
		this.stoppedByAdmin = true;
		this.isStop = true;
		taskLog.setIsAlive(false);
		taskLog.setStopDateTime(DateTime.getDateTime());
	}
	
	public boolean getStatus() {
		return this.isStop;
	}
}