package za.co.hellogroup.bas.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.hellogroup.bas.dao.EmailDomain;
import za.co.hellogroup.bas.dao.FileDomain;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class EmailHandler implements Runnable {
	final static Logger logger = LoggerFactory.getLogger(EmailHandler.class);

	private Properties properties = null;
	private EmailDomain emailDomain;
	private String toAddress;
	private String subject;
	private String message;
	private List<FileDomain> fileList;

	/**
	 * send email
	 * 
	 * @param emailDomain EmailDomain
	 * @param subject
	 * @param message
	 * @param fileList
	 * @return sent or not
	 */
	public void setEmailConfig(EmailDomain emailDomain, String toAddress, String subject, String message, List<FileDomain> fileList) {
		try {
			// 1. sets SMTP server properties
			properties = new Properties();
			properties.put("mail.smtp.host", emailDomain.getHost());
			properties.put("mail.smtps.port", emailDomain.getPort());
			properties.put("mail.user", emailDomain.getUser());
			properties.put("mail.password", emailDomain.getPassword());
			properties.put("mail.smtps.auth", emailDomain.isAuth());
			properties.put("mail.smtp.starttls.enable", emailDomain.isStartTls());
			properties.put("mail.smtps.ssl.trust", emailDomain.isSslTrust());
			properties.put("mail.smtp.timeout", emailDomain.getTimeout());
			properties.put("mail.smtp.connectiontimeout", emailDomain.getConnectiontimeout());

			this.emailDomain = emailDomain;
			this.toAddress = toAddress;
			this.subject = subject;
			this.message = message;
			this.fileList = fileList;
		} catch (Exception e) {
			logger.info("setEmailConfig Exception error : {}", e.getMessage());
		}
	}

	/**
	 * send email
	 */
	public void run() {
		try {
			// 1. creates a new session with an authenticator
			Authenticator auth = getAuthenticator(emailDomain.getUser(), emailDomain.getPassword());
			Session session = Session.getInstance(properties, auth);

			// 2. creates a new e-mail message
			Message msg = new MimeMessage(session);

			// 3-1. set header fields
			// msg.setFrom(new InternetAddress(username));
			msg.setFrom(new InternetAddress(emailDomain.getFrom()));
//			InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
//			Address[] toAddresses = { new InternetAddress(toAddress) };
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			// 3-2. creates multi-part
			Multipart multipart = new MimeMultipart();
			BodyPart bodyPart = new MimeBodyPart();

			// 3-2-1. creates message part (message content)
			bodyPart.setContent(message, "text/html");
			multipart.addBodyPart(bodyPart);

			// 3-2-2. adds attachments
			if (fileList != null && fileList.size() > 0) {
				// creates message part (file content)
				DataHandler dh = null;
				for (FileDomain file : fileList) {
					bodyPart = new MimeBodyPart();

					dh = new DataHandler(new ByteArrayDataSource(file.getFileContent(), "text/plain"));
					bodyPart.setFileName(file.getFileNameOrigin());
					bodyPart.setDataHandler(dh);
					multipart.addBodyPart(bodyPart);
				}
			}

			// 3-3. sets the multi-part as e-mail's content
			msg.setContent(multipart);

			// 4. sends the e-mail
			Transport.send(msg);
		} catch (AddressException ae) {
			logger.info("EmailHandler AddressException error : {}",
					ae.getMessage());
		} catch (MessagingException me) {
			logger.info("EmailHandler MessagingException error : {}",
					me.getMessage());
		} catch (Exception e) {
			logger.info("EmailHandler Exception error : {}", e.getMessage());
		}
	}

	private static Authenticator getAuthenticator(String username, String password) {
		final String un = username;
		final String pw = password;
		if (un == null) {
			return null;
		}
		return new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(un, pw);
			}
		};
	}
}