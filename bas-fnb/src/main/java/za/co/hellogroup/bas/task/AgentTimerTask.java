package za.co.hellogroup.bas.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.hellogroup.bas.util.DateTime;

import java.util.*;

public final class AgentTimerTask extends TimerTask {
    final static Logger logger = LoggerFactory.getLogger(AgentTimerTask.class);

	private List<Job> jobList;
	private String taskName;
	private Date startTime;
	private long period = 0L;

	public AgentTimerTask() {
		jobList = new ArrayList<Job>();
	}

	public void setList(List<Job> jobList) {
		this.jobList = jobList;
	}

	public List<Job> getList() {
		return jobList;
	}

	public void run() {
		logger.info("AgentTimerTask list size : {}", jobList.size());
		logger.info(DateTime.getCurrentDateTime() +" - start running "+ this.taskName);
		JobHandler.getInstance().executeJobList(jobList);
	}

	public Job get(int index) {
		return jobList.get(index);
	}

	public Job getElement(int index) {
		return get(index);
	}

	public void add(Job element) {
		jobList.add(element);
	}

	public void addElement(Job element) {
		add(element);
	}

	public Job set(int index, Job element) {
		return jobList.set(index, element);
	}

	public Job setElement(int index, Job element) {
		return set(index, element);
	}

	public Job remove(int index) {
		return jobList.remove(index);
	}

	public Job removeElement(int index) {
		return remove(index);
	}

	public Job[] toArray() {
		return jobList.toArray(new Job[0]);
	}

	public Job[] toArrayElement() {
		return toArray();
	}

	public Iterator<Job> iterator() {
		return jobList.iterator();
	}

	public void clear() {
		jobList.clear();
	}

	public void addAll(Collection<Job> collection) {
		jobList.addAll(collection);
	}

	public void sort() {
		Job[] sortJobs = toArray();
		Arrays.sort(sortJobs);
		clear();
		addAll(Arrays.asList(sortJobs));
	}

	public void sortReverse() {
		Job[] sortJobs = toArray();
		Arrays.sort(sortJobs, Collections.reverseOrder());
		clear();
		addAll(Arrays.asList(sortJobs));
	}

	public int size() {
		return jobList.size();
	}

	public int indexOf(Job bucket) {
		return jobList.indexOf(bucket);
	}

	public int addUnique(Job bucket) {
		int i = indexOf(bucket);
		if (-1 >= i) {
			add(bucket);
			i = size();
		}
		return i;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskName() {
		return taskName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date firstTime) {
		this.startTime = firstTime;
	}

	public long getPeriod() {
		return period;
	}

	public void setPeriod(long period) {
		this.period = period;
	}
}