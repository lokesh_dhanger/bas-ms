package za.co.hellogroup.bas.service;

public interface ReceiptItService {
    public boolean transferTransactionData(String msgText) throws Exception;
}