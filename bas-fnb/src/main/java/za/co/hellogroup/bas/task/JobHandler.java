package za.co.hellogroup.bas.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.hellogroup.bas.model.TaskLog;
import za.co.hellogroup.bas.util.DateTime;
import za.co.hellogroup.bas.util.LogUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class JobHandler {
    final static Logger logger = LoggerFactory.getLogger(JobHandler.class);

	private static JobHandler jobHandler;

	private boolean executeJob(Job job) {
		return executeJob(job, false);
	}

	private boolean executeJob(Job job, boolean isSkip) {
		boolean executable = false;
		Class<?> cls = null;
		Method method = null;
		TaskLog taskLog = new TaskLog();

		try {
			cls = job.getJob().getClass();
			logger.info(">>>>>>>>> executeJob Job Start Time : {}", DateTime.getDateTime());

			if (isSkip) {
				logger.info(">>> Job Result : Skip");
			} else {
				logger.info(">>>>>>>>> executeJob Job method name1 : {}, {}", cls.getName(), job.getMethodName());
				method = ReflectHandler.getSupportedMethod(cls, job.getMethodName(), new Class[] { String.class });
				logger.info(">>>>>>>>> executeJob Job method name2 : {}, {}", method.getName(), job.getTaskName());
				taskLog = (TaskLog) method.invoke(job.getJob(), new Object[] { job.getTaskName() });

//				logger.info(">>> executeJob Job Result Code : {}", returnMap.getString(Constants.JOB_MESSAGE_CODE));
//				logger.info(">>> executeJob Job Result Desc : {}", returnMap.getString(Constants.JOB_MESSAGE_CODE));
				if (taskLog != null) {
					logger.info(">>> executeJob Job Started Time : {}, {}", method.getName(), taskLog.getStartDateTime());
					logger.info(">>> executeJob Job Ended Time : {}, {}", method.getName(), taskLog.getStopDateTime());
				}
			}

			logger.info(">>>>>>>>> executeJob Job End Time : {}", DateTime.getDateTime());

			executable = true;
		} catch (NoSuchMethodException nsme) {
			logger.info(">>>>>>>>> executeJob no suche method error : {}", LogUtil.getExceptionLog(nsme));
		} catch (IllegalAccessException iae) {
			logger.info(">>>>>>>>> executeJob illegal access error : {}", LogUtil.getExceptionLog(iae));
		} catch (InvocationTargetException ite) {
			logger.info(">>>>>>>>> executeJob invocation target error : {}", LogUtil.getExceptionLog(ite));
		} catch (Exception e) {
			logger.info(">>>>>>>>> executeJob error : {}", LogUtil.getExceptionLog(e));
		} finally {
			logger.info(">>>>>>>>> job finished");
		}
		return executable;
	}

	public void executeJobList(List<Job> jobList) {
		executeJobList(jobList, false);
	}

	public void executeJobList(List<Job> jobList, boolean isExecuted) {
		logger.info("executeJobList in...");

		if (null != jobList) {
			Job job = null;
			boolean isPreviousJobSuccess = true;

			logger.info("executeJobList Start Time : {}", DateTime.getDateTime());
			logger.info("executeJobList JobHandler.executeJobList size : {}", jobList.size());

			int jobListSize = jobList.size();
			for (int i = 0; i < jobListSize; i++) {
				job = jobList.get(i);
//				logger.info("executeJobList Start Time : {}, {}", job.getTaskName(), job.getJob().getParam().get(Constants.LOG_NAME)).getStartDateTime());

				if (0 == i) {
					isPreviousJobSuccess = executeJob(job);
				} else {
					if (isExecuted) {
						job.setStandalone(false);
					}
					if (job.isStandalone()) {
						if (isPreviousJobSuccess) {
							isPreviousJobSuccess = executeJob(job);
						} else {
							isPreviousJobSuccess = executeJob(job, true);
						}
					} else {
						isPreviousJobSuccess = executeJob(job);
					}
				}
				if (!isPreviousJobSuccess) {
					logger.info("executeJobList failed.");
				}
			}
			logger.info("executeJobList End Time : {}", DateTime.getDateTime());
		}
	}

	public static JobHandler getInstance() {
		if (null == jobHandler) {
			jobHandler = new JobHandler();
		}
		return jobHandler;
	}

	public boolean isExistMethod(Object obj, String methodName) {
		boolean isExist = false;
		Class<?> cls = obj.getClass();
		Method[] methods = ReflectHandler.getSupportedMethods(cls);

		for (int i = 0; i < methods.length; i++) {
			if (methodName.equals(methods[i].getName())) {
				isExist = true;
				break;
			}
		}

		return isExist;
	}

	private JobHandler() {
	}
}
