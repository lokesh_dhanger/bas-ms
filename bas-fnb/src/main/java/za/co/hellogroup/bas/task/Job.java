package za.co.hellogroup.bas.task;

public class Job {
	private Object job;
	private String methodName;
	private String taskName;
//	private MessageMap messageMap;
	private boolean isStandalone;

	public Object getJob() {
		return job;
	}

	public void setJob(Object job) {
		this.job = job;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

/*
	public MessageMap getParam() {
		return messageMap;
	}

	public void setParam(MessageMap messageMap) {
		this.messageMap = messageMap;
	}
*/
	public boolean isStandalone() {
		return isStandalone;
	}

	public void setStandalone(boolean isStandalone) {
		this.isStandalone = isStandalone;
	}

	public Job(Object job, String methodName, String taskName, boolean isStandalone) {
		this.job = job;
		this.methodName = methodName;
		this.taskName = taskName;
//		this.messageMap = messageMap;
		this.isStandalone = isStandalone;
	}

	public Job() {
		clear();
	}

	public void clear() {
		job = null;
		methodName = null;
//		messageMap = null;
		isStandalone = false;
	}
}
