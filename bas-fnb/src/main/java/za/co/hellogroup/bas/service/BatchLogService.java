package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.dao.BatchLog;

public interface BatchLogService {

    public BatchLog selectBatchLog(BatchLog batchLog) throws Exception;
    public BatchLog insertBatchLog(BatchLog batchLog) throws Exception;

}
