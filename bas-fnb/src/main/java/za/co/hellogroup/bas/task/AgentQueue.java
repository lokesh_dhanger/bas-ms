package za.co.hellogroup.bas.task;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class AgentQueue {
	private Object obj;
	private Class<?> classType;
	private Method method = null;
	private List<Object> objList = new ArrayList<Object>();

	public AgentQueue(Class<?> classType, Method m) {
		Class<?>[] parameterTypes = m.getParameterTypes();
		if (!(Modifier.isStatic(m.getModifiers())
				&& m.getReturnType() == boolean.class && parameterTypes[0] == classType
				&& parameterTypes[1] == classType && parameterTypes.length == 2)) {
			throw new RuntimeException("illegal signature");
		}
		method = m;
		this.classType = classType;
		obj = Array.newInstance(classType, 0);
	}

	public AgentQueue(Class<?> classType) {
		this.classType = classType;
		obj = Array.newInstance(classType, 0);
	}

	public AgentQueue add(Object element) {
		if (!classType.isInstance(element)) {
			throw new RuntimeException("illegal arg type");
		}
		if (!contains(element)) {
			objList.add(element);
		}
		return this;
	}

	public boolean contains(Object obj) {
		if (null == method) {
			return objList.contains(obj);
		} else {
			for (int i = 0; i < objList.size(); i++) {
				try {
					Object[] apl = { obj, objList.get(i) };
					Boolean rv = (Boolean) method.invoke(obj, apl);
					if (rv.booleanValue()) {
						return true;
					}
				} catch (Exception _e) {
					throw new RuntimeException(_e);
				}
			}
			return false;
		}
	}

	public Object elementAt(int i) {
		return objList.get(i);
	}

	public boolean isEmpty() {
		return objList.size() == 0;
	}

	public Object remove() {
		return objList.remove(0);
	}

	public int size() {
		return objList.size();
	}

	public Object[] toArray() {
		return objList.toArray((Object[]) obj);
	}

	public String toString(String separator) {
		StringBuffer result = new StringBuffer("");
		for (int i = 0; i < objList.size(); i++) {
			result.append(objList.get(i));
			if (i < objList.size() - 1) {
				result.append(separator);
			}
		}
		return result.toString();
	}
}
