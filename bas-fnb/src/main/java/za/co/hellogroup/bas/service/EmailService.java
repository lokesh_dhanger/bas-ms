package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.dao.FileDomain;

import java.util.List;

public interface EmailService {
    public boolean sendEmail(String recipient, String subject, String content, List<FileDomain> fileList);
}
