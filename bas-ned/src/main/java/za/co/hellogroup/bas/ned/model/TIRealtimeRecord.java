package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TIRealtimeRecord")
@XmlAccessorType(XmlAccessType.FIELD)
public class TIRealtimeRecord {
	
	private String CISNumber;
	private String ProfileNumber;
	private String Product;
	private String InstanceNumber;
	private String InstanceName;
	private String TransactionKey;
	private String ProcessKey;
	private String ResendKey;
	private String DestinationKey;
	private String Account;
	private String TranType;
	private String Channel;
	private String Amount;
	private String DebitCredit;
	private String Date;
	private String Time;
	private String AccountBookBalance;
	private String AccountAvailableBalance;
	private String UserRef;

	public String getCISNumber() {
		return CISNumber;
	}
	public void setCISNumber(String cISNumber) {
		CISNumber = cISNumber;
	}
	public String getProfileNumber() {
		return ProfileNumber;
	}
	public void setProfileNumber(String profileNumber) {
		ProfileNumber = profileNumber;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getInstanceNumber() {
		return InstanceNumber;
	}
	public void setInstanceNumber(String instanceNumber) {
		InstanceNumber = instanceNumber;
	}
	public String getInstanceName() {
		return InstanceName;
	}
	public void setInstanceName(String instanceName) {
		InstanceName = instanceName;
	}
	public String getTransactionKey() {
		return TransactionKey;
	}
	public void setTransactionKey(String transactionKey) {
		TransactionKey = transactionKey;
	}
	public String getProcessKey() {
		return ProcessKey;
	}
	public void setProcessKey(String processKey) {
		ProcessKey = processKey;
	}
	public String getResendKey() {
		return ResendKey;
	}
	public void setResendKey(String resendKey) {
		ResendKey = resendKey;
	}
	public String getDestinationKey() {
		return DestinationKey;
	}
	public void setDestinationKey(String destinationKey) {
		DestinationKey = destinationKey;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getTranType() {
		return TranType;
	}
	public void setTranType(String tranType) {
		TranType = tranType;
	}
	public String getChannel() {
		return Channel;
	}
	public void setChannel(String channel) {
		Channel = channel;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getDebitCredit() {
		return DebitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		DebitCredit = debitCredit;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	public String getAccountBookBalance() {
		return AccountBookBalance;
	}
	public void setAccountBookBalance(String accountBookBalance) {
		AccountBookBalance = accountBookBalance;
	}
	public String getAccountAvailableBalance() {
		return AccountAvailableBalance;
	}
	public void setAccountAvailableBalance(String accountAvailableBalance) {
		AccountAvailableBalance = accountAvailableBalance;
	}
	public String getUserRef() {
		return UserRef;
	}
	public void setUserRef(String userRef) {
		UserRef = userRef;
	}
	@Override
	public String toString() {
		return "TIRealtimeRecord [CISNumber=" + CISNumber + ", ProfileNumber="
				+ ProfileNumber + ", Product=" + Product + ", InstanceNumber="
				+ InstanceNumber + ", InstanceName=" + InstanceName
				+ ", TransactionKey=" + TransactionKey + ", ProcessKey="
				+ ProcessKey + ", ResendKey=" + ResendKey + ", DestinationKey="
				+ DestinationKey + ", Account=" + Account + ", TranType="
				+ TranType + ", Channel=" + Channel + ", Amount=" + Amount
				+ ", DebitCredit=" + DebitCredit + ", Date=" + Date + ", Time="
				+ Time + ", AccountBookBalance=" + AccountBookBalance
				+ ", AccountAvailableBalance=" + AccountAvailableBalance
				+ ", UserRef=" + UserRef + "]";
	}
}