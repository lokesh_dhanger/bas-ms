package za.co.hellogroup.bas.ned.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs  //enables SOAP Web Service features
@Configuration //configures the annotation-driven Spring-WS programming model.
public class WebServiceConfig extends WsConfigurerAdapter {

    /**
     *  create a MessageDispatcherServlet which is used for handling SOAP requests
     *
     * @param applicationContext
     * @return
     */
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/nedpti/*");
    }

    @Bean(name = "ITIWebDistribution")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema schema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("ITIWebDistribution");
        wsdl11Definition.setLocationUri("/nedpti");
        wsdl11Definition.setTargetNamespace("http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution");
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema TIWebDistributionSchema() {
        return new SimpleXsdSchema(new ClassPathResource("wsdl/TIWebDistribution_2013-11-01.xsd"));
    }

}
