package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "DistributeMsgRq")
public class DistributeMsgRq {
	private Content content;

	@XmlElement(name = "Content")
	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "DistributeMsgRq [content=" + content.toString() + "]";
	}
}