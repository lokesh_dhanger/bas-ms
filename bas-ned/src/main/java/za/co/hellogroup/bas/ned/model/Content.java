package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Content")
@XmlAccessorType(XmlAccessType.FIELD)
public class Content {
	@XmlElement(name = "Format")
	private String Format;
	@XmlElement(name = "SecurityProxyType")
	private String SecurityProxyType;
	@XmlElement(name = "DestinationKey")
	private String DestinationKey;

	@XmlElement(name = "TransformedData")
	private TransformedData TransformedData;

	public String getFormat() {
		return Format;
	}
	public void setFormat(String format) {
		Format = format;
	}
	public String getSecurityProxyType() {
		return SecurityProxyType;
	}
	public void setSecurityProxyType(String securityProxyType) {
		SecurityProxyType = securityProxyType;
	}
	public String getDestinationKey() {
		return DestinationKey;
	}
	public void setDestinationKey(String destinationKey) {
		DestinationKey = destinationKey;
	}
	public TransformedData getTransformedData() {
		return TransformedData;
	}
	public void setTransformedData(TransformedData transformedData) {
		TransformedData = transformedData;
	}
	@Override
	public String toString() {
		return "Content [Format=" + Format + ", SecurityProxyType="
				+ SecurityProxyType + ", DestinationKey=" + DestinationKey
				+ ", TransformedData=" + TransformedData.toString() + "]";
	}
}