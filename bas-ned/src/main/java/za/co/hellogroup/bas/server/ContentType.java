
package za.co.hellogroup.bas.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Content_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Content_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SecurityProxyType" type="{http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution}SecurityProxyType_Type"/&gt;
 *         &lt;element name="DestinationKey" type="{http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution}DestinationKey_Type"/&gt;
 *         &lt;element name="Format" type="{http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution}Format_Type"/&gt;
 *         &lt;element name="TransformedData" type="{http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution}TransformedData_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content_Type", namespace = "http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution", propOrder = {
    "securityProxyType",
    "destinationKey",
    "format",
    "transformedData"
})
//@XmlRootElement (name = "Content")
public class ContentType {

    @XmlElement(name = "SecurityProxyType", required = true)
    protected String securityProxyType;
    @XmlElement(name = "DestinationKey")
    protected long destinationKey;
    @XmlElement(name = "Format", required = true)
    protected String format;
    @XmlElement(name = "TransformedData", required = true)
    protected String transformedData;

    /**
     * Gets the value of the securityProxyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityProxyType() {
        return securityProxyType;
    }

    /**
     * Sets the value of the securityProxyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityProxyType(String value) {
        this.securityProxyType = value;
    }

    /**
     * Gets the value of the destinationKey property.
     * 
     */
    public long getDestinationKey() {
        return destinationKey;
    }

    /**
     * Sets the value of the destinationKey property.
     * 
     */
    public void setDestinationKey(long value) {
        this.destinationKey = value;
    }

    /**
     * Gets the value of the format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the value of the format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormat(String value) {
        this.format = value;
    }

    /**
     * Gets the value of the transformedData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransformedData() {
        return transformedData;
    }

    /**
     * Sets the value of the transformedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransformedData(String value) {
        this.transformedData = value;
    }

}
