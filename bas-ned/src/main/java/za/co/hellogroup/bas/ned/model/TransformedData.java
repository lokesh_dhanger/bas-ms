package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TransformedData")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransformedData {
	@XmlElement(name = "TIRequest")
	private TIRequest tIRequest;

	public TIRequest gettIRequest() {
		return tIRequest;
	}

	public void settIRequest(TIRequest tIRequest) {
		this.tIRequest = tIRequest;
	}

	@Override
	public String toString() {
		return "TransformedData [tIRequest=" + tIRequest.toString() + "]";
	}
}