package za.co.hellogroup.bas.ned.endpoint;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import za.co.hellogroup.bas.ned.model.TIRealtimeRecord;
import za.co.hellogroup.bas.ned.model.TIRequest;
import za.co.hellogroup.bas.ned.server.DistributeMsgRqType;
import za.co.hellogroup.bas.ned.server.DistributeMsgRsType;
import za.co.hellogroup.bas.ned.service.TransactionService;
import za.co.hellogroup.bas.util.EncoderDecoder;
import za.co.hellogroup.bas.util.XmlUtil;

@Endpoint
public class TIWebDistributionSoapPortImpl {

    final static Logger logger = LoggerFactory.getLogger(TIWebDistributionSoapPortImpl.class);

    private static final String NAMESPACE_URI = "http://contracts.it.nednet.co.za/services/business-execution/2013-11-01/TIWebDistribution";

    @Autowired
    private TransactionService transactionService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "DistributeMsgRq")
    @ResponsePayload
    public DistributeMsgRsType execute(@RequestPayload DistributeMsgRqType parameters) {
        return distributeMsg(parameters);
    }

    private DistributeMsgRsType distributeMsg(DistributeMsgRqType parameters) {
        logger.info(">>>=============================================================");
        logger.info("Executing operation distributeMsg");
        DistributeMsgRsType _return = new DistributeMsgRsType();

        try {
            // Decode message body
            String xmlTransactionData = EncoderDecoder.decodeBase64(parameters.getContent().getTransformedData());
            logger.info("Decoded parameters : {}", xmlTransactionData);

            // Convert xml string to object
            TIRequest requestXML = XmlUtil.unmarshal(xmlTransactionData, TIRequest.class);
            TIRealtimeRecord recordData = requestXML.gettIRealtimeRecord();
            transactionService.transferTransactionData(recordData, xmlTransactionData);
        } catch (java.lang.Exception ex) {
            logger.error("Exception = {}", ex);
            ex.printStackTrace();
        }
        _return.setResultCode("R00");
        return _return;
    }

}
