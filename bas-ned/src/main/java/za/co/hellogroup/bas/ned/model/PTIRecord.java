package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PTIRecord")
@XmlAccessorType(XmlAccessType.FIELD)
public class PTIRecord {
//	private String cISNumber;
	private String profileNumber;
	private String product;
	private String instanceNumber;
//	private String instanceName;
	private String transactionKey;
	private String processKey;
	private String resendKey;
	private String destinationKey;
	private String account;
	private String tranType;
	private String channel;
	private String amount;
	private String debitCredit;
	private String date;
	private String time;
	private String accBalance;
//	private String accountBookBalance;
//	private String accountAvailableBalance;
	private String userRef;

	@XmlElement (name = "ProfileNumber")
	public String getProfileNumber() {
		return profileNumber;
	}
	public void setProfileNumber(String profileNumber) {
		this.profileNumber = profileNumber;
	}
	@XmlElement (name = "Product")
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	@XmlElement (name = "InstanceNumber")
	public String getInstanceNumber() {
		return instanceNumber;
	}
	public void setInstanceNumber(String instanceNumber) {
		this.instanceNumber = instanceNumber;
	}
	@XmlElement (name = "TransactionKey")
	public String getTransactionKey() {
		return transactionKey;
	}
	public void setTransactionKey(String transactionKey) {
		this.transactionKey = transactionKey;
	}
	@XmlElement (name = "ProcessKey")
	public String getProcessKey() {
		return processKey;
	}
	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}
	@XmlElement (name = "ResendKey")
	public String getResendKey() {
		return resendKey;
	}
	public void setResendKey(String resendKey) {
		this.resendKey = resendKey;
	}
	@XmlElement (name = "DestinationKey")
	public String getDestinationKey() {
		return destinationKey;
	}
	public void setDestinationKey(String destinationKey) {
		this.destinationKey = destinationKey;
	}
	@XmlElement (name = "Account")
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	@XmlElement (name = "TranType")
	public String getTranType() {
		return tranType;
	}
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	@XmlElement (name = "Channel")
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	@XmlElement (name = "Amount")
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	@XmlElement (name = "DebitCredit")
	public String getDebitCredit() {
		return debitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}
	@XmlElement (name = "Date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@XmlElement (name = "Time")
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@XmlElement (name = "AccBalance")
	public String getAccBalance() {
		return accBalance;
	}
	public void setAccBalance(String accBalance) {
		this.accBalance = accBalance;
	}
	@XmlElement (name = "UserRef")
	public String getUserRef() {
		return userRef;
	}
	public void setUserRef(String userRef) {
		this.userRef = userRef;
	}
}