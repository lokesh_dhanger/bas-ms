package za.co.hellogroup.bas.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "DistributeMsgRs")
public class DistributeMsgRs {
	private String ResultCode;

	public String getResultCode() {
		return ResultCode;
	}

	public void setResultCode(String resultCode) {
		ResultCode = resultCode;
	}

	@Override
	public String toString() {
		return "DistributeMsgRs [ResultCode=" + ResultCode + "]";
	}
}