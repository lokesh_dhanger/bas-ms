package za.co.hellogroup.bas.ned.service;

import za.co.hellogroup.bas.ned.model.TIRealtimeRecord;

public interface TransactionService {
    public boolean transferTransactionData(TIRealtimeRecord msgObj, String msgText) throws Exception;
}