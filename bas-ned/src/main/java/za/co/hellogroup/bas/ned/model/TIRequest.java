package za.co.hellogroup.bas.ned.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TIRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class TIRequest {
	@XmlElement(name = "TIRealtimeRecord")
	private TIRealtimeRecord tIRealtimeRecord;

	public TIRealtimeRecord gettIRealtimeRecord() {
		return tIRealtimeRecord;
	}

	public void settIRealtimeRecord(TIRealtimeRecord tIRealtimeRecord) {
		this.tIRealtimeRecord = tIRealtimeRecord;
	}

	@Override
	public String toString() {
		return "TIRequest [tIRealtimeRecord=" + tIRealtimeRecord.toString() + "]";
	}
}