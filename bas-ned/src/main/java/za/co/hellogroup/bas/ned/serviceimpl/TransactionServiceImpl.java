package za.co.hellogroup.bas.ned.serviceimpl;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.commons.CommonConstants;
import za.co.hellogroup.bas.dao.inoutlog.InOutLog;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;
import za.co.hellogroup.bas.enums.BasType;
import za.co.hellogroup.bas.enums.StatusCode;
import za.co.hellogroup.bas.model.BaseRequestDataModel;
import za.co.hellogroup.bas.model.BaseResponseModel;
import za.co.hellogroup.bas.ned.model.TIRealtimeRecord;
import za.co.hellogroup.bas.model.TransactionMasterModel;

import za.co.hellogroup.bas.service.CommonService;
import za.co.hellogroup.bas.service.InOutLogService;
import za.co.hellogroup.bas.service.SystemPropertyService;
import za.co.hellogroup.bas.ned.service.TransactionService;
import za.co.hellogroup.bas.util.DateTime;
import za.co.hellogroup.bas.util.HttpUtil;
import za.co.hellogroup.bas.util.LogUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("transactionService")
@Scope("prototype")
public class TransactionServiceImpl implements TransactionService {

	final static Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

//	@Autowired
//	ReceiptItServiceDB receiptItServiceDB;

	@Autowired
	private CommonService commonService;

	@Autowired
	private SystemPropertyService systemPropertyService;

	@Autowired
	private InOutLogService inOutLogService;

	@Override
	public boolean transferTransactionData(TIRealtimeRecord msgObj, String msgText) throws Exception {
		logger.info(">>>>>> Starting transferTransactionData : {}", msgText);

		boolean isSuccess = false;
		int cnt = 0;
	   	InOutLog logDomain = new InOutLog();
	   	logDomain.setBasType(BasType.NED.getValue());
	   	logDomain.setRequestText(msgText);
	   	logDomain.setStatusCode(StatusCode.READY.getValue());
	   	logDomain.setCreatedBy(CommonConstants.SYS_USERNAME);
	   	logDomain.setCreatedOn(DateTime.getPastDate(new Date(), 0));
	   	logDomain.setUpdatedBy(logDomain.getCreatedBy());
	   	logDomain.setUpdatedOn(logDomain.getCreatedOn());
	   	//TransactionServiceDB transactionService = null;

		try {
			//transactionService = ApplicationContextHolder.getContext().getBean(TransactionServiceDB.class);


			//// 1. Pre-task
			// 1-1. Insert request data into a log table (Bank)
			//cnt = transactionService.insertInOutLog(logDomain);
			logDomain = inOutLogService.insertInOutLog(logDomain);
			
	   		// 1-2. Get transaction data from bank
   			TransactionMasterModel transactionModel = new TransactionMasterModel();
   			transactionModel.setBasType(BasType.NED.getValue());
   			transactionModel.setCreatedBy(CommonConstants.SYS_USERNAME);
   			transactionModel.setCreatedOn(DateTime.getLocalDateTime());

			String orgRefNumber = msgObj.getUserRef();
			logger.info("Original Reference Number:{}",orgRefNumber);

			String filteredRefNumber = cleanReferenceNumber(orgRefNumber);
			logger.info("After removing other string, new reference number would be:{}",filteredRefNumber);
			transactionModel.setRefNumber(filteredRefNumber); //save cleaned up ref number

			//if original-reference-number is not equal to filtered-reference-number
			// then only send org-reference-number.
			if (!orgRefNumber.equals(filteredRefNumber))
				transactionModel.setOrgRefNumber(orgRefNumber); //save org_ref_number as it is.

		   	transactionModel.setTransactionDate(DateTime.convertDateTimeToString(DateTime.getDateFromFormat((msgObj.getDate() + msgObj.getTime()).replaceAll(":", ""), CommonConstants.FORMAT_DATE_TIME_YMDHMSS)));
		   	transactionModel.setTransactionAmount(new BigDecimal(msgObj.getAmount()));
		   	transactionModel.setTransactionId(msgObj.getTransactionKey());
		   	transactionModel.setChannel(msgObj.getChannel());
		   	if ("RC".equals(msgObj.getDebitCredit())) {
		   		transactionModel.setDebitCredit(CommonConstants.DEBIT_CREDIT_D);
		   	} else if ("RD".equals(msgObj.getDebitCredit())) {
		   		transactionModel.setDebitCredit(CommonConstants.DEBIT_CREDIT_C);
		   	} else {
		   		transactionModel.setDebitCredit(msgObj.getDebitCredit().substring(msgObj.getDebitCredit().length()-1, msgObj.getDebitCredit().length()));
		   	}
		   	transactionModel.setTransactionTypeMain(CommonConstants.INDICATOR_ANY);
		   	transactionModel.setTransactionTypeSub(msgObj.getTranType());
			transactionModel.setIndicator(msgObj.getDebitCredit());

		   	// 1-3. Set log data with parsed data
			logDomain.setRefNumber(transactionModel.getRefNumber());
			logDomain.setChannel(transactionModel.getChannel());
			logDomain.setTransactionId(transactionModel.getTransactionId());
			logDomain.setTransactionAmount(transactionModel.getTransactionAmount());
			logDomain.setTransactionDate(transactionModel.getTransactionDate());
			logDomain.setDebitCredit(transactionModel.getDebitCredit());
			logDomain.setTransactionTypeMain(transactionModel.getTransactionTypeMain());
			logDomain.setTransactionTypeSub(transactionModel.getTransactionTypeSub());
			logDomain.setIndicator(transactionModel.getIndicator());

   			logger.info(">>>>>> transactionMasterModel : {}", transactionModel.toString());

			// 1-4. Validate reference number
//			CommonServiceDB commonServiceDB = ApplicationContextHolder.getContext().getBean(CommonServiceDB.class);
//			boolean isValidRefNumber = commonServiceDB.validateRefNumber(transactionModel.getRefNumber());
			boolean isValidRefNumber = commonService.validateRefNumber(transactionModel.getRefNumber());
			if (isValidRefNumber) {
				// 1-5. Get system properties from database
				//SystemPropertyServiceDB systemPropertyServiceDB = ApplicationContextHolder.getContext().getBean(SystemPropertyServiceDB.class);

				List<String> propertyKeyList = new ArrayList<String>();
				propertyKeyList.add(CommonConstants.BAS_USERNAME);
				propertyKeyList.add(CommonConstants.BAS_PASSWORD);
				propertyKeyList.add(CommonConstants.BAS_URL_FOR_TRANSACTION);
				Map<String, SystemProperty> propertyKeyMap = systemPropertyService.selectSystemPropertyList(CommonConstants.SYSTEM_CODE_COMMON, propertyKeyList);

				if (propertyKeyMap != null && propertyKeyMap.size() == propertyKeyList.size()) {
					//// 2. Biz process
					// 2-1. Set authentication and transaction data
					BaseRequestDataModel<TransactionMasterModel> dataModel = new BaseRequestDataModel<TransactionMasterModel>();
					dataModel.setUsername(propertyKeyMap.get(CommonConstants.BAS_USERNAME).getPropertyValue());
					dataModel.setPassword(propertyKeyMap.get(CommonConstants.BAS_PASSWORD).getPropertyValue());
					transactionModel.setId(logDomain.getId());
					dataModel.setData(transactionModel);
						
					// 2-2. Send transaction data to BAS core
					String responseData = HttpUtil.sendPostJson(propertyKeyMap.get(CommonConstants.BAS_URL_FOR_TRANSACTION).getPropertyValue(), dataModel);
					if (responseData != null) {
						// 2-4. Parsing response data
						try {
							Gson gson = new Gson();
							BaseResponseModel responseModel = gson.fromJson(responseData, BaseResponseModel.class);
								
							if (responseModel != null) {
								if (responseModel.getResponseCode().equals(CommonConstants.MC_SUCCESS_BAS)) {
									logDomain.setStatusCode(StatusCode.SENT.getValue());
								} else {
									logDomain.setStatusCode(StatusCode.FAIL.getValue());
								}
								logDomain.setMessageCode(responseModel.getResponseCode());
								logDomain.setMessageText(responseModel.getResponseMessage());
							} else {
	   							logDomain.setStatusCode(StatusCode.FAIL.getValue());
	   							logDomain.setStatusText("[Double check] Failed to get response from BAS, so please double check if BAS got this data or not");
							}
							isSuccess = true;
						} catch (Exception e) {
							logDomain.setStatusCode(StatusCode.FAIL.getValue());
							logDomain.setStatusText("[Double check] Failed to parse response data from BAS core : "+ responseData);
						}
					} else {
	   					logDomain.setStatusCode(StatusCode.FAIL.getValue());
	   					logDomain.setStatusText("[Double check] Failed to get response data from BAS");
					}
				} else {
					logDomain.setStatusCode(StatusCode.FAIL.getValue());
					logDomain.setStatusText("Could not get system property data. Please check these system property data : "+ CommonConstants.BAS_USERNAME +", "+ CommonConstants.BAS_PASSWORD +", "+ CommonConstants.BAS_URL_FOR_TRANSACTION);
				}
			} else {
				logDomain.setStatusCode(StatusCode.FAIL.getValue());
	   			logDomain.setStatusText("Invalid reference number : "+ transactionModel.getRefNumber());
			}
		} catch (Exception e) {
			logger.error("transferTransactionData error : {}", LogUtil.getExceptionLog(e));
		} finally {
//			if (cnt > 0) {
				// 1-4. Update request data into a log table (Bank)
				logDomain.setUpdatedOn(DateTime.getPastDate(new Date(), 0));
				inOutLogService.updateInOutLog(logDomain);
//			}
			logger.info("Ending transferTransactionData......");
		}

		return isSuccess;
	}

	/**
	 * This is for getting actual reference number from alpha-numeric reference number...
	 * @param referenceStr
	 * @return
	 */
	public String cleanReferenceNumber(String referenceStr){
		String filteredStr = referenceStr;
		if(StringUtils.isNotEmpty(referenceStr)){
			referenceStr = referenceStr.replaceAll("[a-zA-Z\\s]","");
			if(referenceStr.contains(CommonConstants.HP_REF_PREFIX) || referenceStr.contains(CommonConstants.PNP_REF_PREFIX)){
				if(referenceStr.contains(CommonConstants.HP_REF_PREFIX) && !referenceStr.startsWith(CommonConstants.HP_REF_PREFIX)){
					referenceStr = referenceStr.substring(referenceStr.indexOf(CommonConstants.HP_REF_PREFIX));
				}
				if(referenceStr.contains(CommonConstants.PNP_REF_PREFIX) && !referenceStr.startsWith(CommonConstants.PNP_REF_PREFIX)){
					referenceStr = referenceStr.substring(referenceStr.indexOf(CommonConstants.PNP_REF_PREFIX));
				}
				if((referenceStr.length() == CommonConstants.HP_REF_LENGTH && (referenceStr.startsWith(CommonConstants.HP_REF_PREFIX) || referenceStr.startsWith(CommonConstants.PNP_REF_PREFIX)))
						|| (referenceStr.length() == CommonConstants.BANK_REF_LENGTH && (referenceStr.startsWith(CommonConstants.BANK_REF_PREFIX) || referenceStr.startsWith(CommonConstants.PNP_BANK_REF_PREFIX)))){
					filteredStr = referenceStr;
				}
			}
			filteredStr = filteredStr.trim();
		}
		return filteredStr;
	}
}