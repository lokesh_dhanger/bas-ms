package za.co.hellogroup.bas.dao.systemproperty.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;

@Repository
public interface SystemPropertyRepository extends JpaRepository<SystemProperty, Long> {

}
