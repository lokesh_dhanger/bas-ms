package za.co.hellogroup.bas.util;

import java.math.BigDecimal;

public class CurrencyConverter {
	private static int ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;

	private int ccScale = 3;
	
	public void setScale (int scale) {
		this.ccScale = scale;
	}

	/**
	 * get formatted value
	 * @param amt
	 * @param scale
	 * @return
	 */
	public BigDecimal getFormattedValue(BigDecimal amt, int scale) {
		this.ccScale = scale;

		return rounding(amt);
	}

	/**
	 * sum two values
	 * @param amt1
	 * @param amt2
	 * @return
	 */
	public BigDecimal getSum(BigDecimal amt1, BigDecimal amt2) {
		return rounding(amt1).add(rounding(amt2));
	}

	/**
	 * subtract amt2 from amt1
	 * @param amt1
	 * @param amt2
	 * @return
	 */
	public BigDecimal getSubtract(BigDecimal amt1, BigDecimal amt2) {
		return rounding(amt1).subtract(rounding(amt2));
	}

	/**
	 * mutiply two values
	 * @param amt1
	 * @param amt2
	 * @return
	 */
	public BigDecimal getMultiply(BigDecimal amt1, BigDecimal amt2) {
		return rounding(rounding(amt1).multiply(rounding(amt2)));
	}

	/**
	 * amt2 divides into amt1 
	 * @param amt1
	 * @param amt2
	 * @return
	 */
	public BigDecimal getDivide(BigDecimal amt1, BigDecimal amt2) {
		return rounding(amt1).divide(rounding(amt2), ROUNDING_MODE);
	}

	public BigDecimal getAverage(BigDecimal amt1, BigDecimal amt2) {
		return getSum(amt1, amt2).divide(new BigDecimal("2"), ROUNDING_MODE);
	}

	private BigDecimal rounding (BigDecimal amt) {
		return amt.setScale(this.ccScale, ROUNDING_MODE);
	}
}
