package za.co.hellogroup.bas.dao.auditLog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.auditLog.AuditLog;


@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {

}
