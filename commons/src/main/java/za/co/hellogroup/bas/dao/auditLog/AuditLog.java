package za.co.hellogroup.bas.dao.auditLog;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "audit_log")
public class AuditLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "entity_name")
    private String entity_name;

    @Column(name = "transactionid")
    private String transactionid;

    @Column(name = "ref_number")
    private String refNumber;

    @Column(name = "data")
    private String data;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditLog auditLog = (AuditLog) o;
        return Objects.equals(id, auditLog.id) && Objects.equals(createdBy, auditLog.createdBy) && Objects.equals(createdOn, auditLog.createdOn) && Objects.equals(entity_name, auditLog.entity_name) && Objects.equals(transactionid, auditLog.transactionid) && Objects.equals(refNumber, auditLog.refNumber) && Objects.equals(data, auditLog.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdBy, createdOn, entity_name, transactionid, refNumber, data);
    }

}
