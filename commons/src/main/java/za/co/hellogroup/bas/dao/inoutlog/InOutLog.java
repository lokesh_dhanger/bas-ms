package za.co.hellogroup.bas.dao.inoutlog;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "inout_log")
public class InOutLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ref_number")
    private String refNumber;

    @Column(name = "bas_type")
    private String basType;

    @Column(name = "channel")
    private String channel;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "transaction_amount")
    private BigDecimal transactionAmount;

    @Column(name = "transaction_date")
    private String transactionDate;

    @Column(name = "debit_credit")
    private String debitCredit;

    @Column(name = "transaction_type_main")
    private String transactionTypeMain;

    @Column(name = "transaction_type_sub")
    private String transactionTypeSub;

    @Column(name = "indicator")
    private String indicator;

    @Column(name = "status_code")
    private int statusCode;

    @Column(name = "status_text")
    private String statusText;

    @Column(name = "message_code")
    private String messageCode;

    @Column(name = "message_text")
    private String messageText;

    @Column(name = "request_text")
    private String requestText;

    @Column(name = "response_text")
    private String responseText;

    @Column(name = "log_text")
    private String logText;

    @Column(name = "description")
    private String description;

    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @PreUpdate
    protected void onUpdate() {
        this.updatedOn = new Date();
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InOutLog inOutLog = (InOutLog) o;
        return statusCode == inOutLog.statusCode && Objects.equals(id, inOutLog.id) && Objects.equals(refNumber, inOutLog.refNumber) && Objects.equals(basType, inOutLog.basType) && Objects.equals(channel, inOutLog.channel) && Objects.equals(transactionId, inOutLog.transactionId) && Objects.equals(transactionAmount, inOutLog.transactionAmount) && Objects.equals(transactionDate, inOutLog.transactionDate) && Objects.equals(debitCredit, inOutLog.debitCredit) && Objects.equals(transactionTypeMain, inOutLog.transactionTypeMain) && Objects.equals(transactionTypeSub, inOutLog.transactionTypeSub) && Objects.equals(indicator, inOutLog.indicator) && Objects.equals(statusText, inOutLog.statusText) && Objects.equals(messageCode, inOutLog.messageCode) && Objects.equals(messageText, inOutLog.messageText) && Objects.equals(requestText, inOutLog.requestText) && Objects.equals(responseText, inOutLog.responseText) && Objects.equals(logText, inOutLog.logText) && Objects.equals(description, inOutLog.description) && Objects.equals(createdOn, inOutLog.createdOn) && Objects.equals(updatedOn, inOutLog.updatedOn) && Objects.equals(createdBy, inOutLog.createdBy) && Objects.equals(updatedBy, inOutLog.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, refNumber, basType, channel, transactionId, transactionAmount, transactionDate, debitCredit, transactionTypeMain, transactionTypeSub, indicator, statusCode, statusText, messageCode, messageText, requestText, responseText, logText, description, createdOn, updatedOn, createdBy, updatedBy);
    }
}
