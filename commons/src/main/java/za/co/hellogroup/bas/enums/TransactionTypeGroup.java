package za.co.hellogroup.bas.enums;

public enum TransactionTypeGroup {
	CREDIT("C"), DEBIT("D"), REVERSAL("R"), ADJUSTMENT("A"), RETRY("R"), OTHER("O");
	
	private String transactionTypeGroup;
	
	TransactionTypeGroup(String ttg) {
		this.transactionTypeGroup = ttg;
	}
	
	public String getValue() {
		return this.transactionTypeGroup;
	}
}