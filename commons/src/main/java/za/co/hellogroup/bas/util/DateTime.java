package za.co.hellogroup.bas.util;


import org.apache.commons.lang3.StringUtils;
import za.co.hellogroup.bas.commons.CommonConstants;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTime {

	/**
	 * Get other second date
	 * 
	 * @param seconds
	 *            day count < Example > Today : 2016-06-24 11:20:30 getOtherSecond(10) returns 2016-06-24 11:20:40
	 * @return
	 */
	public final static String getOtherSecond(int seconds) {
		DateFormat df = new SimpleDateFormat(CommonConstants.FORMAT_DATE_TIME_DEFAULT_SLASH);
		Date date = new Date();
		date.setTime(date.getTime() + (seconds) * (1000));
		return df.format(date);
	}

	public static Date getDateFromFormat(String theDateTime, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(theDateTime);
	}
	
	public static String getLocalDate() {
		return getLocalDate(null);
	}

	public static String getLocalDate(String timezone) {
		DateFormat df = new SimpleDateFormat(CommonConstants.FORMAT_DATE_DEFAULT_HYPHEN);

		if (timezone != null) {
			df.setTimeZone(TimeZone.getTimeZone(timezone));
		}

		Date date = new Date();
		return df.format(date);
	}

	public static String getLocalDateTime() {
		return getLocalDateTime(null);
	}

	public static String getLocalDateTime(String timezone) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat(
				CommonConstants.FORMAT_DATE_TIME_DEFAULT_HYPHEN);

		if (timezone != null) {
			df.setTimeZone(TimeZone.getTimeZone(timezone));
		}

		return df.format(date);
	}

	public static Date formatDateStr(String dateStr, String format) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.parse(dateStr);
	}

	/**
	 * This is for getting pastDate by given hours...
	 * @param curDate
	 * @param hours
	 * @return
	 */
	public static Date getPastDate(Date curDate, int hours){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.add(Calendar.HOUR_OF_DAY, -hours);
		return calendar.getTime();
	}

	/**
	 * Get other date using days
	 * 
	 * @param days
	 *            day count < Example > Today : 2015-10-02 getotherDate(-2)
	 *            returns 2015-09-30 getotherDate(3) returns 2015-10-05
	 * @return
	 */
	public final static String getOtherDate(int days) {
		DateFormat df = new SimpleDateFormat(CommonConstants.FORMAT_DATE_DEFAULT_HYPHEN);
		Date date = new Date();
		date.setTime(date.getTime() + (days) * (24 * 60 * 60 * 1000));
		return df.format(date);
	}

	/**
	 * Get difference second between two time (Don't use two long term date.)
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static int getDiffSecond(Date from, Date to) {
		int diffSecond = (int) (from.getTime() - to.getTime()) / 1000;

		return diffSecond;
	}

	public static String getDateTime() {
		return getDateTime(CommonConstants.FORMAT_DATE_TIME_DEFAULT_SLASH);
	}

	public static String getDateTime(String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(new Date());
	}

	public static String getDateTime(long date) {
		return getDateTime(date);
	}

	public static String getCurrentDateTime() {
		return getDateTime();
	}

	public static Date getDateFromFormat(String theDateTime) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.FORMAT_DATE_TIME_DEFAULT_SLASH);
		return sdf.parse(theDateTime);
	}

	public static String convertDateToOtherFormat(String dateTime, String sourceFormat, String targetFormat) {
		String convertedDateTime = null;
		if (StringUtils.isNotBlank(dateTime)
		&&  StringUtils.isNotBlank(dateTime)
		&&  StringUtils.isNotBlank(dateTime)) {
			try {
				SimpleDateFormat sourceDateFormat = new SimpleDateFormat(sourceFormat);
				SimpleDateFormat targetDateFormat = new SimpleDateFormat(targetFormat);
				Date date = sourceDateFormat.parse(dateTime);
				convertedDateTime = targetDateFormat.format(date);
			} catch (Exception e) {
			}
		}

		return convertedDateTime;
	}

	public static String convertDateToString(Date dateTime) {
		return convertDateToString(dateTime, CommonConstants.FORMAT_DATE_DEFAULT_HYPHEN);
	}

	public static String convertDateTimeToString(Date dateTime) {
		return convertDateToString(dateTime, CommonConstants.FORMAT_DATE_TIME_DEFAULT_HYPHEN);
	}

	public static String convertDateToString(Date dateTime, String format) {
		String convertedDateTime = null;
		if (dateTime != null && StringUtils.isNotBlank(format)) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(format);
				convertedDateTime = dateFormat.format(dateTime);
			} catch (Exception e) {
			}
		}

		return convertedDateTime;
	}

	public static String convertXmlGregorianToDate(XMLGregorianCalendar xmlCalendar, String format) {
		String convertedDateTime = null;

		if (xmlCalendar != null) {
			convertDateToString(xmlCalendar.toGregorianCalendar().getTime(), format);
		}
		return convertedDateTime;
	}
}