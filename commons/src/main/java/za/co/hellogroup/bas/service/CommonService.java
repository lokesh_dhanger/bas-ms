package za.co.hellogroup.bas.service;

public interface CommonService {

    public boolean validateRefNumber(String refNumber);
    public String selectTransactionTypeGroup(String basType, String transactionTypeMain, String transactionTypeSub);

}
