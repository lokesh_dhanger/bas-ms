package za.co.hellogroup.bas.enums;

public enum StatusCode {
	READY(1), ING(2), SENT(0), FAIL(9), DROP(4);
	
	private int statusCode;
	
	StatusCode(int sc) {
		this.statusCode = sc;
	}
	
	public int getValue() {
		return this.statusCode;
	}
}