package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class TransactionMasterModel {
	private long id;
	private String refNumber;
	private String orgRefNumber;
	private String basType;
	private String channel;
	private String transactionId;
	private BigDecimal transactionAmount;
	private String transactionDate;
	private String debitCredit;
	private String transactionTypeMain;
	private String transactionTypeSub;
	private String indicator;
	private String createdBy;
	private String createdOn;
}