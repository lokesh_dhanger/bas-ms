package za.co.hellogroup.bas.model;


import javax.validation.Valid;

public class BaseRequestDataModel<T> extends BaseRequestModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5958532979164898324L;
	@Valid
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return super.toString() + " " + data.toString();
    }
}