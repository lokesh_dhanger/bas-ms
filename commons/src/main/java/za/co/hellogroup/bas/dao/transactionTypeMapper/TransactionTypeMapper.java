package za.co.hellogroup.bas.dao.transactionTypeMapper;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


@Getter
@Setter
@ToString
@Entity
@Table(name = "transaction_type_mapper")
public class TransactionTypeMapper implements Serializable {

    @Column(name = "bas_type")
    private String basType;

    @Column(name = "transaction_type_main")
    private String transactionTypeMain;

    @Column(name = "transaction_type_sub")
    private String transactionTypeSub;

    @Column(name = "indicator")
    private boolean indicator;

    @Column(name = "transaction_type_group")
    private String transactionTypeGroup;

    @Column(name = "on_off")
    private int onOff;

    @Column(name = "description")
    private String description;

    @Column(name = "tender_type")
    private String tenderType;

    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @PreUpdate
    protected void onUpdate() {
        this.updatedOn = new Date();
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionTypeMapper that = (TransactionTypeMapper) o;
        return indicator == that.indicator && Objects.equals(basType, that.basType) && Objects.equals(transactionTypeMain, that.transactionTypeMain) && Objects.equals(transactionTypeSub, that.transactionTypeSub) && Objects.equals(transactionTypeGroup, that.transactionTypeGroup) && Objects.equals(onOff, that.onOff) && Objects.equals(description, that.description) && Objects.equals(tenderType, that.tenderType) && Objects.equals(createdOn, that.createdOn) && Objects.equals(updatedOn, that.updatedOn) && Objects.equals(createdBy, that.createdBy) && Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(basType, transactionTypeMain, transactionTypeSub, indicator, transactionTypeGroup, onOff, description, tenderType, createdOn, updatedOn, createdBy, updatedBy);
    }
}
