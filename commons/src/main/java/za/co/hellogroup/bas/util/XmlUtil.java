package za.co.hellogroup.bas.util;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class XmlUtil {
	/**
	 * Convert a string to an object of a given class.
	 *
	 * @param src
	 *            Input string
	 * @param clazz
	 *            Type of object
	 * @return Object of the given type
	 */
	public static <T> T unmarshal(String src, Class<T> clazz) {
		if (src == null) {
			return null;
		}
		return unmarshal(new StringReader(src), clazz);
	}

	/**
	 * Convert the contents of a file to an object of a given class.
	 *
	 * @param clazz
	 *            Type of object
	 * @param f
	 *            File to be read
	 * @return Object of the given type
	 */
	public static <T> T unmarshal(File f, Class<T> clazz) {
		return unmarshal(new StreamSource(f), clazz);
	}

	/**
	 * Convert the contents of a Reader to an object of a given class.
	 *
	 * @param clazz
	 *            Type of object
	 * @param r
	 *            Reader to be read
	 * @return Object of the given type
	 */
	public static <T> T unmarshal(Reader r, Class<T> clazz) {
		return unmarshal(new StreamSource(r), clazz);
	}

	/**
	 * Convert the contents of an InputStream to an object of a given class.
	 *
	 * @param src
	 *            InputStream to be read
	 * @param clazz
	 *            Type of object
	 * @return Object of the given type
	 */
	public static <T> T unmarshal(InputStream src, Class<T> clazz) {
		return unmarshal(new StreamSource(src), clazz);
	}

	/**
	 * Convert the contents of a Source to an object of a given class.
	 *
	 * @param src
	 *            Source to be used
	 * @param clazz
	 *            Type of object
	 * @return Object of the given type
	 */
	public static <T> T unmarshal(Source src, Class<T> clazz) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(clazz);
			Unmarshaller u = ctx.createUnmarshaller();
			return u.unmarshal(src, clazz).getValue();
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
			return null;
		}
	}

	/**
	 * Converts the contents of the string to a List with objects of the given
	 * class.
	 *
	 * @param src
	 *            Input string
	 * @param clazz
	 *            Type to be used
	 * @return List with objects of the given type
	 */
	public static <T> List<T> unmarshalCollection(String src, Class<T> clazz) {
		return unmarshalCollection(new StringReader(src), clazz);
	}

	/**
	 * Converts the contents of the Reader to a List with objects of the given
	 * class.
	 *
	 * @param clazz
	 *            Type to be used
	 * @param r
	 *            Input
	 * @return List with objects of the given type
	 */
	public static <T> List<T> unmarshalCollection(Reader r, Class<T> clazz) {
		return unmarshalCollection(new StreamSource(r), clazz);
	}

	/**
	 * Converts the contents of the InputStream to a List with objects of the
	 * given class.
	 *
	 * @param src
	 *            Input
	 * @param clazz
	 *            Type to be used
	 * @return List with objects of the given type
	 */
	public static <T> List<T> unmarshalCollection(InputStream src,
			Class<T> clazz) {
		return unmarshalCollection(new StreamSource(src), clazz);
	}

	/**
	 * Converts the contents of the Source to a List with objects of the given
	 * class.
	 *
	 * @param src
	 *            Input
	 * @param clazz
	 *            Type to be used
	 * @return List with objects of the given type
	 */
	public static <T> List<T> unmarshalCollection(Source src, Class<T> clazz) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(JAXBCollection.class,
					clazz);
			Unmarshaller u = ctx.createUnmarshaller();
			JAXBCollection<T> collection = u.unmarshal(src, JAXBCollection.class).getValue();
			return collection.getItems();
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
			return null;
		}
	}

	/**
	 * Convert an object to a string.
	 *
	 * @param obj
	 *            Object that needs to be serialized / marshalled.
	 * @return String representation of obj
	 */
	public static <T> String marshal(T obj) {
		if (obj == null) {
			return null;
		}
		StringWriter sw = new StringWriter();
		marshal(obj, sw);
		return sw.toString();
	}

	/**
	 * Convert an object to a string and send it to a Writer.
	 *
	 * @param obj
	 *            Object that needs to be serialized / marshalled
	 * @param wr
	 *            Writer used for outputting the marshalled object
	 */
	public static <T> void marshal(T obj, Writer wr) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
			Marshaller jaxbMarshaller = ctx.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			QName qName = new QName(obj.getClass().getPackage().toString());
			JAXBElement<T> root = new JAXBElement<T>(qName, (Class<T>) obj.getClass(), obj);
			obj.getClass().getPackage().toString();

			jaxbMarshaller.marshal(root, wr);
			// jaxbMarshaller.marshal(obj, wr);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Convert an object to a string and save it to a File.
	 *
	 * @param obj
	 *            Object that needs to be serialized / marshalled
	 * @param f
	 *            Save file
	 */
	public static <T> void marshal(T obj, File f) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
			Marshaller m = ctx.createMarshaller();
			m.marshal(obj, f);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Convert an object to a string and send it to an OutputStream.
	 *
	 * @param obj
	 *            Object that needs to be serialized / marshalled
	 * @param src
	 *            Stream used for output
	 */
	public static <T> void marshal(T obj, OutputStream src) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
			Marshaller m = ctx.createMarshaller();
			m.marshal(obj, src);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Convert a collection to a string.
	 *
	 * @param rootName
	 *            Name of the XML root element
	 * @param c
	 *            Collection that needs to be marshalled
	 * @return String representation of the collection
	 */
	public static <T> String marshal(String rootName, Collection<T> c) {
		StringWriter sw = new StringWriter();
		marshal(rootName, c, sw);
		return sw.toString();
	}

	/**
	 * Convert a collection to a string and sends it to the Writer.
	 *
	 * @param rootName
	 *            Name of the XML root element
	 * @param c
	 *            Collection that needs to be marshalled
	 * @param w
	 *            Output
	 */
	public static <T> void marshal(String rootName, Collection<T> c, Writer w) {
		try {
			// Create context with generic type
			JAXBContext ctx = JAXBContext.newInstance(findTypes(c));
			Marshaller m = ctx.createMarshaller();

			// Create wrapper collection
			JAXBElement element = createCollectionElement(rootName, c);
			m.marshal(element, w);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Convert a collection to a string and stores it in a File.
	 *
	 * @param rootName
	 *            Name of the XML root element
	 * @param c
	 *            Collection that needs to be marshalled
	 * @param f
	 *            Output file
	 */
	public static <T> void marshal(String rootName, Collection<T> c, File f) {
		try {
			// Create context with generic type
			JAXBContext ctx = JAXBContext.newInstance(findTypes(c));
			Marshaller m = ctx.createMarshaller();

			// Create wrapper collection
			JAXBElement element = createCollectionElement(rootName, c);
			m.marshal(element, f);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Convert a collection to a string and sends it to the OutputStream.
	 *
	 * @param rootName
	 *            Name of the XML root element
	 * @param c
	 *            Collection that needs to be marshalled
	 * @param src
	 *            Output
	 */
	public static <T> void marshal(String rootName, Collection<T> c,
			OutputStream src) {
		try {
			// Create context with generic type
			JAXBContext ctx = JAXBContext.newInstance(findTypes(c));
			Marshaller m = ctx.createMarshaller();

			// Create wrapper collection
			JAXBElement element = createCollectionElement(rootName, c);
			m.marshal(element, src);
		} catch (JAXBException jaxbe) {
			jaxbe.printStackTrace();
		}
	}

	/**
	 * Discovers all the classes in the given Collection. These need to be in
	 * the JAXBContext if you want to marshal those objects. Unfortunatly
	 * there's no way of getting the generic type at runtime.
	 *
	 * @param c
	 *            Collection that needs to be scanned
	 * @return Classes found in the collection, including JAXBCollection.
	 */
	protected static <T> Class[] findTypes(Collection<T> c) {
		Set<Class> types = new HashSet<Class>();
		types.add(JAXBCollection.class);
		for (T o : c) {
			if (o != null) {
				types.add(o.getClass());
			}
		}
		return types.toArray(new Class[0]);
	}

	/**
	 * Create a JAXBElement containing a JAXBCollection. Needed for marshalling
	 * a generic collection without a seperate wrapper class.
	 *
	 * @param rootName
	 *            Name of the XML root element
	 * @param c
	 * @return JAXBElement containing the given Collection, wrapped in a
	 *         JAXBCollection.
	 */
	protected static <T> JAXBElement createCollectionElement(String rootName,
			Collection<T> c) {
		JAXBCollection collection = new JAXBCollection(c);
		return new JAXBElement<JAXBCollection>(new QName(rootName),
				JAXBCollection.class, collection);
	}
}