package za.co.hellogroup.bas.dao.transactionTypeSubMapper;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "transaction_type_sub_mapper")
public class TransactionTypeSubMapper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "partner_code")
    private String partnerCode;

    @Column(name = "bas_type")
    private String basType;

    @Column(name = "transaction_type_sub")
    private String transactionTypeSub;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionTypeSubMapper that = (TransactionTypeSubMapper) o;
        return Objects.equals(id, that.id) && Objects.equals(partnerCode, that.partnerCode) && Objects.equals(basType, that.basType) && Objects.equals(transactionTypeSub, that.transactionTypeSub);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, partnerCode, basType, transactionTypeSub);
    }
}
