package za.co.hellogroup.bas.enums;

public enum BasType {
	FNB("FNB"), NED("NED"), STD("STD");
	
	private String basType;
	
	BasType(String basType) {
		this.basType = basType;
	}
	
	public String getValue() {
		return this.basType;
	}
}