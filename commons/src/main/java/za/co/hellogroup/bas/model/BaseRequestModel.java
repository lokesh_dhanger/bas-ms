package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@ToString
public class BaseRequestModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9201930168451572450L;

	protected String basKey;
	protected String username;
    protected String token;
    protected String password;
    protected String sigan = "1";  // Default : 1

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BaseRequestModel that = (BaseRequestModel) o;
		return Objects.equals(basKey, that.basKey) && Objects.equals(username, that.username) && Objects.equals(token, that.token) && Objects.equals(password, that.password) && Objects.equals(sigan, that.sigan);
	}

	@Override
	public int hashCode() {
		return Objects.hash(basKey, username, token, password, sigan);
	}
}