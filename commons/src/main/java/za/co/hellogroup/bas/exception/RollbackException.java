package za.co.hellogroup.bas.exception;

import org.springframework.transaction.TransactionException;

public class RollbackException extends TransactionException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5138555282505733447L;

	public RollbackException(String message) {
		super(message);
	}

	public RollbackException(String message, Throwable cause) {
		super(message, cause);
	}
}
