package za.co.hellogroup.bas.serviceimpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.dao.inoutlog.InOutLog;
import za.co.hellogroup.bas.dao.inoutlog.repository.InOutLogRepository;
import za.co.hellogroup.bas.service.InOutLogService;

@Service("inOutLogService")
public class InOutLogServiceImpl implements InOutLogService {

    @Autowired
    private InOutLogRepository inOutLogRepository;

    @Override
    public InOutLog insertInOutLog(InOutLog inOutLog) {
        return inOutLogRepository.save(inOutLog);
    }

    @Override
    public InOutLog updateInOutLog(InOutLog inOutLog) {
        return inOutLogRepository.save(inOutLog);
    }

    @Override
    public InOutLog selectInOutLog(long id) {
        return inOutLogRepository.findById(id).get();
    }
}
