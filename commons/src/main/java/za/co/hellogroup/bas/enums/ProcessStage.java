package za.co.hellogroup.bas.enums;

public enum ProcessStage {
	GREENHOUSE(1), ENQUIRY(5), CONFIRM(7), REVISION(3);
	
	private int processStage;
	
	ProcessStage(int ps) {
		this.processStage = ps;
	}
	
	public int getValue() {
		return this.processStage;
	}
}