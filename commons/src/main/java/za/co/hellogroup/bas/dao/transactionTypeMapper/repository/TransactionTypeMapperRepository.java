package za.co.hellogroup.bas.dao.transactionTypeMapper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.transactionTypeMapper.TransactionTypeMapper;

@Repository
public interface TransactionTypeMapperRepository extends JpaRepository<TransactionTypeMapper, Long> {

}
