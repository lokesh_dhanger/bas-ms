package za.co.hellogroup.bas.service;

import za.co.hellogroup.bas.dao.inoutlog.InOutLog;

public interface InOutLogService {

    public InOutLog insertInOutLog(InOutLog inOutLog);
    public InOutLog updateInOutLog(InOutLog inOutLog);
    public InOutLog selectInOutLog(long id);

}
