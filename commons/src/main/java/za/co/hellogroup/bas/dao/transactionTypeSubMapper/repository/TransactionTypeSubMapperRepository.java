package za.co.hellogroup.bas.dao.transactionTypeSubMapper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.transactionTypeSubMapper.TransactionTypeSubMapper;

@Repository
public interface TransactionTypeSubMapperRepository extends JpaRepository<TransactionTypeSubMapper, Long> {

}
