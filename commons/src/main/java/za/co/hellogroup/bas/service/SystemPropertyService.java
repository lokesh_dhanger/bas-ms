package za.co.hellogroup.bas.service;


import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;

import java.util.List;
import java.util.Map;

public interface SystemPropertyService {

    /**
     * select a system property by systemName and propertyName
     * @param systemName
     * @param propertyName
     * @return
     */
    public String selectSystemProperty(String systemName, String propertyName);

    /**
     *
     * get list of system properties by systemCode and propertyName
     *
     * @param systemCode
     * @param propertyName
     * @return
     */
    public List<SystemProperty> selectSystemPropertyProc(String systemCode, String propertyName);

    /**
     * get list of system properrties by systemCode and propertyNames list
     *
     * @param systemCode
     * @param propertyNames
     * @return
     */
    public Map<String, SystemProperty> selectSystemPropertyList(String systemCode, List<String> propertyNames);

    public int insertSystemProperty(SystemProperty systemProperty);
    public int updateSystemProperty(SystemProperty systemProperty);

    /**
     * delete a system property by systemCode and propertyName
     *
     * @param systemCode
     * @param propertyName
     * @return
     */
    public int deleteSystemProperty(String systemCode, String propertyName);

}
