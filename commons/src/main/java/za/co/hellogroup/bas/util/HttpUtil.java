package za.co.hellogroup.bas.util;

import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class HttpUtil {

    public static String sendGet(String stringUrl, Map<String, Object> params) {
        String response = null;
        try {
            URL url = new URL(stringUrl + "?" + setParameters(params));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            response = convertStreamToString(conn.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String sendPostJson(String targetURL, Object params) {
        String response = null;
  		DataOutputStream dos = null;

        try {
    		Gson gson = new Gson();
    		String jsonData = gson.toJson(params);

            URL url = new URL(targetURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            
            // Send post request
    		conn.setDoOutput(true);
    		dos = new DataOutputStream(conn.getOutputStream());

    		dos.writeBytes(jsonData);
    		dos.flush();
    		dos.close();

            response = convertStreamToString(conn.getInputStream());
        } catch (IOException e) {
        	LogUtil.getExceptionLog(e);
        } finally {
        	if (dos != null) {
        		try {
        			dos.close();
        		} catch (Exception e) {}
        	}
        }
        return response;
    }

    public static String setParameters(Map<String, Object> params) {
        try {
            StringBuilder parameters = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (parameters.length() != 0) {
                    parameters.append('&');
                }
                parameters.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                parameters.append('=');
                parameters.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            return parameters.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String convertStreamToString(InputStream inputStream) {
        StringBuffer responseData = new StringBuffer();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String oneLine;

            while ((oneLine = reader.readLine()) != null) {
                responseData.append(oneLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseData.toString();
    }

}
