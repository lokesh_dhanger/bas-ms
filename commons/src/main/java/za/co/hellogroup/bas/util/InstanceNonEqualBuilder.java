package za.co.hellogroup.bas.util;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class InstanceNonEqualBuilder {

	final static Logger logger = LoggerFactory.getLogger(InstanceNonEqualBuilder.class);
	private static final HashMap<String, List<String>> SUPPRESSIBLE_GETTERS = initSuppressibleGetters();
	private static final HashMap<String, String> MODEL_TABLE_MAP = initModeltoTable();

	private static HashMap<String, List<String>> initSuppressibleGetters() {
		HashMap<String, List<String>> hashMap = new HashMap<String, List<String>>();
		
		hashMap.put("za.co.hellogroup.bas.fnb.domain.InOutLogDomain",
				Arrays.asList("Id", "CreatedOn", "CreatedBy", "BranchCode", "BranchListModel", "SignedUsername",
						"SignedRoleCode", "UpdatedBy", "UpdatedOn", "ForeignCreatedOn", "ForeignUpdatedOn", "Class","TransactionDetailsList","RequestText"));
		
		return hashMap;
	}

	private static HashMap<String, String> initModeltoTable() {
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("za.co.hellogroup.bas.fnb.domain.InOutLogDomain", "INOUT_LOG");
		return hashMap;
	}

	public static String nonEqualPropertiesAsJson(Object oldInstance, Object newInstance, Class classIns) throws Exception{

		try {
			if (oldInstance != null && MODEL_TABLE_MAP.containsKey(classIns.getName())) {
				HashMap<String, String> newNonEqualMap = new HashMap<String, String>();
				for (Method method : classIns.getMethods()) {
					if (method.getName().startsWith("get")) {
						String fieldName = method.getName().substring(3, method.getName().length());
						if (!SUPPRESSIBLE_GETTERS.get(classIns.getName()).contains(fieldName)) {
							String oldFieldValue = (method.invoke(oldInstance) != null)
									? method.invoke(oldInstance).toString() : "";
							String newFieldValue = (method.invoke(newInstance) != null)
									? method.invoke(newInstance).toString() : "";
							if (!newFieldValue.equals(oldFieldValue)) {
								newNonEqualMap.put(fieldName, newFieldValue);
							}
						}
					}
				}
				if(newNonEqualMap.size() > 0)
					return new Gson().toJson(newNonEqualMap);
				else
					return null;
			} else {
				if(newInstance !=null)
					return new Gson().toJson(newInstance);
				else
					return null;
			}
		} catch (Exception e) {
			logger.error("Exception at nonEqualPropertiesAsJson", e);
			throw e;
		}
		
	}

}
