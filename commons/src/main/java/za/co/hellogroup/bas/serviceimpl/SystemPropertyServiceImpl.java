package za.co.hellogroup.bas.serviceimpl;


import org.springframework.stereotype.Service;
import za.co.hellogroup.bas.dao.systemproperty.SystemProperty;
import za.co.hellogroup.bas.service.SystemPropertyService;

import java.util.List;
import java.util.Map;

@Service("systemPropertyService")
public class SystemPropertyServiceImpl implements SystemPropertyService {


    @Override
    public String selectSystemProperty(String systemName, String propertyName) {
        return null;
    }

    @Override
    public List<SystemProperty> selectSystemPropertyProc(String systemCode, String propertyName) {
        return null;
    }

    @Override
    public Map<String, SystemProperty> selectSystemPropertyList(String systemCode, List<String> propertyNames) {
        return null;
    }

    @Override
    public int insertSystemProperty(SystemProperty systemProperty) {
        return 0;
    }

    @Override
    public int updateSystemProperty(SystemProperty systemProperty) {
        return 0;
    }

    @Override
    public int deleteSystemProperty(String systemCode, String propertyName) {
        return 0;
    }
}
