package za.co.hellogroup.bas.dao.inoutlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.hellogroup.bas.dao.inoutlog.InOutLog;

@Repository
public interface InOutLogRepository extends JpaRepository<InOutLog, Long> {



}
