package za.co.hellogroup.bas.exception;

public class StopException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6253456621940653889L;

	public StopException() {
		super();
	}

	public StopException(String message) {
		super(message);
	}

	public StopException(Throwable cause) {
		super(cause);
	}

	public StopException(String message, Throwable cause) {
		super(message, cause);
	}

	public StopException(String message, Throwable cause,
							 boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
