package za.co.hellogroup.bas.service;


import za.co.hellogroup.bas.dao.auditLog.AuditLog;

public interface AuditLogService {

    public int insertAuditLog(AuditLog auditLog);

}
