package za.co.hellogroup.bas.util;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class EncoderDecoder {
	public static String encodeBase64(String plainText) {
		if (plainText != null) {
			return DatatypeConverter.printBase64Binary(plainText.getBytes(StandardCharsets.UTF_8));
		} else {
			return null;
		}
	}

	public static String decodeBase64(String encodedValue) {
		if (encodedValue != null) {
			return new String(Base64.getDecoder().decode(encodedValue), StandardCharsets.UTF_8);
		} else {
			return null;
		}
	}
}