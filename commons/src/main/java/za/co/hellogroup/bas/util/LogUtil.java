package za.co.hellogroup.bas.util;


public class LogUtil {
     /**
     * Get exception message
     *
	 * @param e Exception
     * @return header information
     */
    public static String getExceptionLog (Exception e) {
    	StackTraceElement[] ste = e.getStackTrace();
    	
    	StringBuffer sb = new StringBuffer();
    	sb.append(ste[0].getClassName());
    	sb.append(".");
    	sb.append(ste[0].getMethodName());
    	sb.append(" : ");
    	sb.append(ste[0].getFileName());
    	sb.append(" > ");
    	sb.append(ste[0].getLineNumber());
    	sb.append("\nMessage : ");
    	sb.append(e.getMessage());
    	sb.append("\n");
    	
        return sb.toString();
    }
}