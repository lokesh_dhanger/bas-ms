package za.co.hellogroup.bas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

@Getter
@Setter
@ToString
public class BaseResponseModel {

	private String responseCode;
    private String responseMessage;
	private String token;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BaseResponseModel that = (BaseResponseModel) o;
		return Objects.equals(responseCode, that.responseCode) && Objects.equals(responseMessage, that.responseMessage) && Objects.equals(token, that.token);
	}

	@Override
	public int hashCode() {
		return Objects.hash(responseCode, responseMessage, token);
	}
}