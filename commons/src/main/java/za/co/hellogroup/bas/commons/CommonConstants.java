package za.co.hellogroup.bas.commons;

public class CommonConstants {

    // biz
    public static final String INDICATOR_ANY = "ANY";
    public static final String INDICATOR_INVALID = "INV";  // Invaid
    public static final int REF_NUMBER_LENGTH = 14;

    // Message variable
    public static final String LOG_NAME = "AGENT_LOG";
    public static final String JOB_START_DATE_TIME = "JOB_START_DATE_TIME";
    public static final String JOB_END_DATE_TIME = "JOB_END_DATE_TIME";
    public static final String JOB_MESSAGE_CODE = "JOB_MESSAGE_CODE";
    public static final String JOB_MESSAGE_DESC = "JOB_MESSAGE_DESC";
    public static final String RC_SUCCESS = "0";  // Success
    public static final String RC_FAIL = "1";  // FAIL
    public static final String SYSTEM_CODE_COMMON = "BACMMN";
    public static final String DEBIT_CREDIT_D = "D";
    public static final String DEBIT_CREDIT_C = "C";
    public static final String SYS_USERNAME = "basadmin";
    public static final short ON_OFF_ON = 1;
    public static final short ON_OFF_OFF = 0;

    // System property
    public static final String BAS_USERNAME = "bas_username";
    public static final String BAS_PASSWORD = "bas_password";
    public static final String BAS_URL_FOR_AUTHENTICATION = "bas_url_for_authentication";
    public static final String BAS_URL_FOR_TRANSACTION = "bas_url_for_transaction";
    public static final String SA_EMAIL = "sa_email";  // system admin email
    public static final String BAS_BANK_TRAN_ENTITY = "INOUT_LOG";

    // email
    public static final String PROPERTY_NAME_EMAIL_HOST = "email_host";
    public static final String PROPERTY_NAME_EMAIL_PORT = "email_port";
    public static final String PROPERTY_NAME_EMAIL_USER = "email_user";
    public static final String PROPERTY_NAME_EMAIL_PASSWORD = "email_password";
    public static final String PROPERTY_NAME_EMAIL_FROM = "email_from";
    public static final String PROPERTY_NAME_EMAIL_AUTH = "email_auth";
    public static final String PROPERTY_NAME_EMAIL_STARTTLS = "email_starttls";
    public static final String PROPERTY_NAME_EMAIL_SSL_TRUST = "email_ssl_trust";
    public static final String PROPERTY_NAME_EMAIL_TIMEOUT = "email_smtp_timeout";
    public static final String PROPERTY_NAME_EMAIL_CONNECTIONTIMEOUT = "email_smtp_connectiontimeout";
    public static final String PROPERTY_NAME_EXPIRY_SECOND_TOKEN = "expiry_second_token";
    public static final String PROPERTY_NAME_MAIN_DOMAIN = "main_domain";

    // Date Format
    public static final String FORMAT_DATE_DEFAULT_HYPHEN = "yyyy-MM-dd";
    public static final String FORMAT_TIME_DEFAULT_COLON = "HH:mm:ss";
    public static final String FORMAT_DATE_DEFAULT_SLASH = "yyyy/MM/dd";
    public static final String FORMAT_DATE_TIME_DEFAULT_HYPHEN = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_TIME_DEFAULT_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String FORMAT_DATE_TIME_YMDHMSS = "yyyyMMddHHmmssSSS";
    public static final String FORMAT_DATE_TIME_YMDHMS = "yyyyMMddHHmmss";
    public static final String FORMAT_DATE_TIME_YMD = "yyyyMMdd";

    // temporary path to save a file
    public static final String FNB_ROOT_PATH = "/usr/share/jboss/bas/fnb";

    //// FNB MESSAGE definition
    public static final String MSG_HEADER = "H";
    public static final String MSG_BODY = "B";

    //// message code from BAS
    public static final String MC_SUCCESS_BAS = "0";  // success


    // common
    public static final String MC_404 = "404";  // Not found data

    // Parameter
    public static final String MC_400 = "400";  // Invalid data error
    public static final String MC_406 = "406";  // invalid reference number/not acceptable
    // Application error
    public static final String MC_500 = "500";  // Application general error
    // System general error
    public static final String MC_503 = "503";  // System error
    // Database general error
    public static final String MC_507 = "507";  // Database error

    // agent management
    public static final String MC_800 = "800";  // Failed to start server
    public static final String MC_801 = "801";  // Failed to stop server

    public static final String MC_732 = "732";  // Failed to create bank audit log.

    public static final String MC_733 = "733";  // Failed to get bank transaction audit data.

    // hp transaction
    public static final String MC_740 = "740";  // Failed to get hp transaction data
    public static final String MC_741 = "741";  // Failed to update HP transaction data
    public static final String MC_742 = "742";  // This HP transaction had already been processed
    public static final String MC_743 = "743";  // This HP transaction is busy now. Please try it later
    public static final String MC_744 = "744";  // Unmatched id and reference number
    public static final String MC_745 = "745";  // Invalid request for HP transaction revision
    public static final String MC_746 = "746";  // Failed to create HP transaction audit data
    public static final String MC_747 = "747";  // Failed to get HP transaction audit data

    // recon transaction
    public static final String MC_750 = "750";  // Failed to get Recon Data
    public static final String MC_751 = "751";  // Failed to update Recon Data
    public static final String RC_752 = "752";  // Failed to create export file

    // daemon of sending data to HP
    public static final String MC_700 = "700";  // Failed to update target transaction data
    public static final String MC_701 = "701";  // Failed to update master transaction data for enquiry
    public static final String MC_702 = "702";  // Failed to update details transaction data for enquiry
    public static final String MC_703 = "703";  // Failed to update master transaction data for confirmation
    public static final String MC_704 = "704";  // Failed to update details transaction data for confirmation


    //NationalityId doesn't match.
    public static final String MC_508 = "508"; // Failed to update HP transaction : NationalityId doesn't match.
    public static final String MC_509 = "509"; // Failed to update HP transaction : New transaction is already matched.

    //HP Reference Prefix
    public static final String HP_REF_PREFIX = "11429";
    public static final String PNP_REF_PREFIX = "11786";
    //HP Reference Length
    public static final int HP_REF_LENGTH = 14;

    //Bank Reference Prefix
    public static final String BANK_REF_PREFIX = "11429786";
    public static final String PNP_BANK_REF_PREFIX = "11786786";
    //Bank Reference Length
    public static final int BANK_REF_LENGTH = 16;

    // authentication
    public static final String MC_100 = "100";  // invalid auth parameters
    public static final String MC_101 = "101";  // invalid username
    public static final String MC_102 = "102";  // invalid password
    public static final String MC_103 = "103";  // invalid token
    public static final String MC_104 = "104";  // failed to create a token
    public static final String MC_105 = "105";  // failed to delete a token
    public static final String MC_106 = "106";  // failed to update password
    public static final String MC_108 = "108";  // system data error in auth process
    public static final String MC_109 = "109";  // error in auth process
    public static final int MINIMUM_PW_LENGTH = 8;

    public static final String EMAIL_TEMPLATE_USER_FORGOT_PASSWORD = "user_forgot_password";

    // Pagination
    public static final String PAGINATION_F = "F";  // (F)irst
    public static final String PAGINATION_P = "P";  // (P)revious
    public static final String PAGINATION_N = "N";  // (N)ext
    public static final String PAGINATION_L = "L";  // (L)ast
    public static final String PAGINATION_A = "A";  // (A)ll

    public static final String PAGINATION_TOTAL_COUNT = "totalCount";  // total count
    public static final String PAGINATION_CURR_PAGE_NO = "currPageNo";  // current page number
    public static final String PAGINATION_LIST_DATA = "listData";  // list data

    // bank transaction
    public static final String MC_730 = "730";  // Failed to get bank transaction data
    public static final String MC_731 = "731";  // Failed to update bank transaction data

    public static final String HP_TRAN_ENTITY = "TRANSACTION_DETAILS";
    public static final String BANK_TRAN_ENTITY = "INOUT_LOG";

    public static final String PNP_PREFIX = "11786";
    public static final String HP_PREFIX = "11429";
    public static final String BANK_REF_NUMBER_REG_EX = "^(11429)[0-9]{11}$";
    public static final int HP_PREFIX_LENGTH = 5;

    public static final String GROUP_CODE_BANK_INFO = "BABANKCD";
    public static final String GROUP_CODE_LOG_STATUS_CODE = "BASTTSLG";
    public static final String GROUP_CODE_TRANS_STATUS_CODE = "BASTTSCD";
    public static final String GROUP_CODE_TRANSACTION_TYPE = "BATRNSTP";
    public static final String GROUP_CODE_PROCESS_STAGE = "BAPROCST";
    public static final String GROUP_CODE_RECON_STATUS_CODE = "BASTTSRG";
    public static final String[] SUPPRESSIBLE_TRANS_STATUS_UI = {"2","99"};

    // file extension
    public static final String FILE_EXT_PDF = "pdf";
    public static final String FILE_EXT_CSV = "csv";
    public static final String FILE_EXT_EXCEL_1997_2003 = "xls";
    public static final String FILE_EXT_EXCEL_2007_2010 = "xlsx";
    public static final String FILE_EXT_TXT = "txt";

    // temporary path to save a file
    public static final String TEMP_FILE_PATH = "../temp/";


}
